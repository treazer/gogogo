﻿package  {
	
	import flash.utils.Timer;
	import flash.text.engine.GraphicElement;
	import flash.text.engine.TabAlignment;
	
	public class Simulation {

		private var m_main:Main;
		private var m_gameStateManager:GameStateManager;
		private var m_spawner:Vector.<Spawn>;
		private var m_player:Player;
		private var m_level:Level;
		private var m_gameObjects:Array;
		private var m_gyroVec:Vec2;
		private var m_trigger:Vector.<Trigger>;
		private var m_messages:Vector.<Message>;
		private var m_triggermap:Array;
		private var m_batteryCount:int = 0;
		private var m_screenShakeTimer:int = 0;
		private var m_screenShakeIntensity:Number = 0;
		private var m_endingMessageCooldown:int = 0;
		private var m_gameEnds:int = 0;
		private var m_gameEnds2:Boolean = false;
		public var m_levelnr;
		private var m_gameEndsCountdown:int =0;
		private var m_startEnd:Boolean = false;
		
		
		public function Simulation(gameStateManager:GameStateManager, main:Main,level:int) {
			m_levelnr = level;
			m_gameObjects = new Array;
			m_main = main;
			m_gameStateManager = gameStateManager;
			m_spawner = new Vector.<Spawn>();
			m_trigger = new Vector.<Trigger>();
			m_messages = new Vector.<Message>();
			m_player = new Player(new Vec2(0,0),this);
			m_level = new Level(level,this);
			var playerLegs = new PlayerLegs(m_player,this);
			m_gameObjects.push(playerLegs);
			m_player.setPlayerLegs(playerLegs);
			m_gameObjects.push(m_player);
			m_gyroVec = new Vec2(0,0);
			m_triggermap = new Array();
			m_screenShakeTimer=0;
		}

		public function step():void
		{
			if (m_level.hasLoaded)
			{
				if (!m_messages.some(messageShowing))
				{
					m_endingMessageCooldown--;
					m_screenShakeTimer--;
					updateSpawner();
					updateTrigger();
					updateGameObjects();
					leaveGame();
				}
			}
		}
		
		public function messageShowing(msg:Message, index:int, messages:Vector.<Message>):Boolean
		{
			return msg.isDone;
		}
		
		public function isMessageShowing():Boolean
		{
			for each (var msg:Message in m_messages)
			{
				if (msg.isShowing)
					return true;
			}
			return false;
		}
		
		public function skipMessage()
		{
			for each (var msg:Message in m_messages)
			{
				if (msg.isShowing)
				{
					msg.done();
				}
			}
		}
		
		public function updateGameObjects():void
		{
			for (var i:int=0;i<m_gameObjects.length;i++)
			{
				if (m_gameObjects[i].step()) {
					m_gameObjects.splice(i,1);
					i--;
				}
			}
		}
		
		public function get gameObjects():Array
		{
			return m_gameObjects;
		}

		public function updateSpawner():void {
			for each (var spawner:Spawn in m_spawner) {
				spawner.step();
			}
		}
		
		public function updateTrigger():void {
			for each (var trigger:Trigger in m_trigger) {
				trigger.step();
			}
		}
		
		public function increaseBatteryCount():void
		{
			m_batteryCount++;
		}
		
		public function get player():Player
		{
			return m_player;
		}

		public function get spawner():Vector.<Spawn>
		{
			return m_spawner;
		}
		
		public function get trigger():Vector.<Trigger>
		{
			return m_trigger;
		}
		
		public function get messages():Vector.<Message>
		{
			return m_messages;
		}
		
		public function get level():Level
		{
			return m_level;
		}
		
		public function get gyroVec():Vec2
		{
			return m_gyroVec;
		}
		
		public function setGyroVec(value:Vec2)
		{
			m_gyroVec = value;
		}
		
		public function get main():Main
		{
			return m_main;
		}
		
		public function get soundManager():SoundManager
		{
			return m_gameStateManager.soundManager;
		}
		
		public function endGame():void
		{
			if (m_batteryCount<3 && m_player.health>0)
			{
				if (m_endingMessageCooldown<0) 
				{
					//inform player
					var newMessage = new Message("Wait Nanoman! You cannot leave yet! You need at least 3 batteries!",-1);
					messages.push(newMessage);
					newMessage.showMessage();
					m_endingMessageCooldown=100;
				}
			}
			//player wins
			if (m_batteryCount>2)
			{
				m_gameEnds= 1;
				//m_gameStateManager.endGame();
			}
			
			//player dies
			if (m_player.health<=0)
			{
				m_gameEnds2= true;
				m_startEnd=true;
			}
		}
		
		public function leaveGame():void
		{
			if (m_startEnd)
				m_gameEndsCountdown++;
			if (m_gameEndsCountdown>30)
			{
				m_gameEnds= 2;
			}
		}
		
		public function get triggermap():Array
		{
			return m_triggermap;
		}
		
		public function get batteryCount():int
		{
			return m_batteryCount;
		}
		
		public function get screenShakeTimer():int
		{
			return m_screenShakeTimer;
		}
		
		public function get screenShakeIntensity():Number
		{
			return m_screenShakeIntensity;
		}
		
		public function setScreenShake(duration:int,intensity:Number)
		{
			m_screenShakeTimer = duration;
			m_screenShakeIntensity = intensity;
		}
		
		public function get isEnding():int
		{
			return m_gameEnds;
		}
		
		public function get isEnding2():Boolean
		{
			return m_gameEnds2;
		}
	}
}
