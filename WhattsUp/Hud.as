﻿package  {
	
	import flash.display.MovieClip;
	import flash.geom.Rectangle;
	import flash.display.Graphics;
	import flash.geom.Matrix;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.geom.ColorTransform;
	
	public class Hud extends MovieClip {

		private var m_offset:Vec2;
		private var m_player:Player;
		private var m_graphicsManager:GraphicsManager;
		private var m_renderHealthValue:Number = 0;
		private var m_hitIndicator:BitmapData;
		private var m_hitIndicatorDrawn:BitmapData;
		
		public function Hud(_graphicsManager:GraphicsManager) {
			// constructor code
			m_graphicsManager = _graphicsManager;
			m_hitIndicator = ImageLoader.getBitmapDataFromName(ImageLoader.IMAGE_PLAYER_HIT);
		}
		
		public function step(_offset:Vec2, _player):void {
			m_offset = _offset;
			m_player = _player;
			this.graphics.clear();
			drawCurrentWeaponIcon();
			drawHeatbar();
			drawPlayerHit();
			drawHealthbar();
			drawBatteries();
		}
		
		public function drawPlayerHit():void
		{
			if (m_renderHealthValue!=(m_player.health/m_player.maxHealth) && m_renderHealthValue!=0)
				m_hitIndicatorDrawn = m_hitIndicator.clone();
			if (m_hitIndicatorDrawn)
			{
				m_hitIndicatorDrawn.colorTransform(new Rectangle(0,0,960,640),new ColorTransform(1,1,1,0.965));
				this.graphics.beginBitmapFill(m_hitIndicatorDrawn);
				this.graphics.drawRect(0, 0, 960, 640);
				this.graphics.endFill();
			}
		}
		
		private function drawHealthbar():void {
			var matrix:Matrix = new Matrix();
			matrix.translate(10, 10);
			m_renderHealthValue = (m_player.health/m_player.maxHealth);
			if (m_renderHealthValue<0) m_renderHealthValue=0;
			this.graphics.beginBitmapFill(ImageLoader.getBitmapDataFromName(ImageLoader.IMAGE_BLUEBAR), matrix);
			this.graphics.drawRect(10, 10, m_renderHealthValue * 250, 16);
			this.graphics.endFill();
			
		}
		
		private function drawHeatbar():void {
			var matrix:Matrix = new Matrix();
			matrix.translate(10, 30);
			this.graphics.beginBitmapFill(ImageLoader.getBitmapDataFromName(ImageLoader.IMAGE_YELLOWBAR), matrix);
			this.graphics.drawRect(10, 30, (m_player.currentWeapon.heatLevel/100) * 250, 16);
			this.graphics.endFill();
		}
		
		private function drawCurrentWeaponIcon():void {
			var matrix:Matrix = new Matrix();
			matrix.translate(10, 50);
			this.graphics.beginBitmapFill(ImageLoader.getBitmapDataFromName(m_graphicsManager.gameStateManager.simulation.player.currentWeapon.image), matrix);
			this.graphics.drawRect(10, 50, 128, 64);
			this.graphics.endFill();
		}
		
		private function drawBatteries():void
		{
			if (m_graphicsManager.gameStateManager.simulation.batteryCount>0)
			{
				var matrix:Matrix = new Matrix();
				matrix.translate(640, 0);
				this.graphics.lineStyle(0, 0x000000,0);
				this.graphics.beginBitmapFill(ImageLoader.getBitmapDataFromName(ImageLoader.IMAGE_BATTERY), matrix);
				this.graphics.drawRect(640, 0, 64*m_graphicsManager.gameStateManager.simulation.batteryCount, 64);
				this.graphics.endFill();
			}
		}

	}
	
}
