﻿package  {
	
	public class Weapon {

		private var cooldown:int = 0;
		private var cooldownMax:int = 0;
		private var heatDecrease:Number = 0;
		private var heatIncrease:Number = 0;
		private var m_heatLevel:Number = 0;
		private var maxHeat:Number = 100;
		private var heatheat:Number = 0; // XD Dat variable name tho :D
		private var weaponType:int = 0;
		private var weaponName:String = "";
		private var readyToShoot:Boolean;
		private var overheated:Boolean;
		private var m_bulletSize:int;
		private var m_bullet:String;
		private var m_bulletRenderSizeX:int;
		private var m_bulletRenderSizeY:int;
		private var m_damage:int;
		private var m_image:String = ImageLoader.IMAGE_WEAPONICON_1;
		private var m_straying:Number = 0;
		private var m_sizeIncrease:Vec2;
		private var m_bulletLife:int;
		
		public function Weapon(giveType:int) {
			// constructor code
			weaponType = giveType;
			defineWeapon();
		}
		
		// get values of name and ready
		public function get name():String {
			return weaponName;
		}
		
		public function get ready():Boolean {
			return readyToShoot;
		}
		
		// Defining the properties of the weapon
		public function defineWeapon():void {
			m_bullet = ImageLoader.IMAGE_BULLET_DEFAULT;
			switch(weaponType) {
				case 0:
					weaponName = "FS80 Blister";
					cooldownMax = 10;
					heatIncrease = 30;
					heatDecrease = 2;
					heatheat = 80;
					m_bulletSize = 10;
					m_damage = 12;
					m_bullet = ImageLoader.IMAGE_BULLET;
					m_bulletRenderSizeX=7;
					m_bulletRenderSizeY=7;
					m_image = ImageLoader.IMAGE_WEAPONICON_1;
					m_straying = 0.1;
					m_sizeIncrease = new Vec2(1,1);
					m_bulletLife=30;
					break;
				
				case 1:
					weaponName = "A.Y.S.";
					cooldownMax = 6;
					heatIncrease = 20;
					heatDecrease = 2;
					heatheat = 70;
					m_bulletSize = 10;
					m_damage = 17;
					m_bulletRenderSizeX=16;
					m_bulletRenderSizeY=16;
					m_image = ImageLoader.IMAGE_WEAPONICON_1;
					m_straying = 0.04;
					m_sizeIncrease = new Vec2(1,1);
					m_bulletLife=30;
					break;
				
				case 2:
					weaponName = "Fry Bye";
					cooldownMax = 1;
					heatIncrease = 5;
					heatDecrease = 2;
					heatheat = 30;
					m_bulletSize = 13;
					m_damage = 5;
					m_bullet = ImageLoader.IMAGE_BULLET_FRYBYE;
					m_bulletRenderSizeX=15;
					m_bulletRenderSizeY=15;
					m_image = ImageLoader.IMAGE_WEAPONICON_2;
					m_straying = 0.35;
					m_sizeIncrease = new Vec2(1.11,1.11);
					m_bulletLife=11;
					break;
				case 3:
					weaponName = "Spitter";
					cooldownMax = 20;
					heatIncrease = 10;
					heatDecrease = 2;
					heatheat = 80;
					m_bulletSize = 24;
					m_damage = 11;
					m_bullet = ImageLoader.IMAGE_BULLET_SPITTER;
					m_bulletRenderSizeX=64;
					m_bulletRenderSizeY=64;
					m_straying = 0.15;
					m_sizeIncrease = new Vec2(1,1);
					m_bulletLife=13;
					break;
				// On undefined weaponType
				default:
					break;
				
			}
			return;
		}
		
		public function get heatLevel():Number
		{
			return m_heatLevel;
		}
		
		public function get bulletSize():int
		{
			return m_bulletSize;
		}
		
		public function checkForShoot():void {
			// Function which will appeal if a lifingObject shoots
			if(m_heatLevel >= maxHeat) {
				overheated = true;
			}
			if((overheated == true)&&(m_heatLevel <= heatheat)) {
				overheated = false;
			}
			if(overheated == true) {
				readyToShoot = false;
			} else if(cooldown >= cooldownMax) {
				if((m_heatLevel + heatIncrease) > maxHeat) {
					overheated = true;
					readyToShoot = false;
					
				} else {
					overheated = false;
					readyToShoot = true;
				}
			} else {
				readyToShoot = false;
			}
			
		}
		
		public function startCooldown():void
		{
			if((m_heatLevel + heatIncrease) > maxHeat) {
				m_heatLevel = maxHeat;
			} else {
				m_heatLevel += heatIncrease;
			}
			cooldown = 0;
		}
		
		public function step():void {
			// 
			if((m_heatLevel - heatDecrease) <= 0) {
				m_heatLevel = 0;
			} else {
				m_heatLevel -= heatDecrease;
			}
			cooldown++;
			
			
		}
		
		public function get bullet():String
		{
			return m_bullet;
		}
		
		public function get bulletRenderSizeX():int
		{
			return m_bulletRenderSizeX;
		}
		
		public function get bulletRenderSizeY():int
		{
			return m_bulletRenderSizeY;
		}
		
		public function get damage():int
		{
			return m_damage;
		}
		
		public function get image():String
		{
			return m_image;
		}
		
		public function get straying():Number
		{
			return m_straying;
		}
		
		public function get sizeIncrease():Vec2
		{
			return m_sizeIncrease;
		}
		
		public function get bulletLife():int
		{
			return m_bulletLife;
		}
	}
	
}
