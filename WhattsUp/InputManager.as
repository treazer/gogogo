﻿package  {
	
	import flash.ui.Keyboard;
	import flash.events.*;
	import flash.display.MovieClip;
	import flash.sensors.Accelerometer;
	import flash.events.AccelerometerEvent;
	import flash.ui.Multitouch;
    import flash.ui.MultitouchInputMode;
	import flash.utils.Timer;
	
	public class InputManager extends MovieClip {
		
		private static const PAUSE_POS:Vec2 = new Vec2(800,20);
		private static const PAUSE_SIZE:Vec2 = new Vec2(64,64);
		
		public var key:Array;
		private var m_main:Main;
		private var m_simulation:Simulation;
		private var m_gameStateManager:GameStateManager;
		private var m_graphicsManager:GraphicsManager;
		private var m_joystick:Joystick;
		private var timer:Timer;
		private var m_gestureVec:Vec2;
		private var m_pause:Pause;
		public function InputManager(main:Main,gameStateManager:GameStateManager) {
			
			m_main = main;
			m_gameStateManager = gameStateManager;
			m_joystick = new Joystick();
			m_pause = new Pause();
			m_gestureVec = new Vec2(0,0);
			key = new Array();
			
			Multitouch.inputMode = MultitouchInputMode.TOUCH_POINT;
			var acc:Accelerometer = new Accelerometer();
			acc.addEventListener(AccelerometerEvent.UPDATE,onChange);
			//m_main.stage.addEventListener(TouchEvent.TOUCH_BEGIN,touchStart);
			//m_main.stage.addEventListener(TouchEvent.TOUCH_END,touchEnd);
			m_main.stage.addEventListener(MouseEvent.MOUSE_DOWN,touchStart);
			m_main.stage.addEventListener(MouseEvent.MOUSE_UP,touchEnd);
			m_main.stage.addEventListener(KeyboardEvent.KEY_DOWN,keyPressed);
			m_main.stage.addEventListener(KeyboardEvent.KEY_UP,keyReleased);
			timer = new Timer(30);
			timer.addEventListener(TimerEvent.TIMER,step);
			timer.start();
		}
		
		public function step(e:TimerEvent)
		{
			m_gameStateManager.step();
			if(m_gameStateManager.gameState == "Running"){
				if (m_graphicsManager.createdMapImage)
				{
					m_pause.visible=true;
				}
			
				if (m_joystick.direction.length!=0)
				{
					m_gameStateManager.simulation.player.shoot(m_joystick.direction,true);
				}
				if(m_gameStateManager.simulation.isEnding > 0){
					
					if(m_gameStateManager.simulation.isEnding == 2){
						m_gameStateManager.creatMenue("GameOver");
					}
					if(m_gameStateManager.simulation.isEnding == 1){
						m_gameStateManager.creatMenue("Win");
					}
					destroyViewsGame();
					m_gameStateManager.removeSimulation()
				}
			}
			if(m_gameStateManager.gameState == "Menu"){
				
			}
			
		}
		
		public function touchStart(e:MouseEvent):void
		{
			if(m_gameStateManager.gameState =="Pause"){
				
				if(e.stageX > 480 - 157 && e.stageX < 480 - 157 + 315 && e.stageY > 400 && e.stageY < 400 + 63){
					m_gameStateManager.creatMenue("Main");
					m_gameStateManager.m_pause.remove();
					destroyViewsGame();
					m_gameStateManager.removeSimulation()
					
				}
				if(e.stageX > 480 - 157 && e.stageX < 480 - 157 + 315 && e.stageY > 300 && e.stageY < 300 + 63){
					m_gameStateManager.pauseToRunning();
				}
				
			}
			if(m_gameStateManager.gameState =="Running"){
				
				//if (e.localX<480 && e.currentTarget!=m_joystick && e.stageX<480) {
				if (m_gameStateManager.simulation.isMessageShowing()) {
					m_gameStateManager.simulation.skipMessage();
				}
				else
				{
					m_gestureVec = new Vec2(e.stageX,e.stageY);
				}
				if(m_pause.isPresst){
					m_gameStateManager.switchToPause();
					m_pause.isPresst = false;
				}
			}
			if(m_gameStateManager.gameState == "Menu"){
				if(1 == m_gameStateManager.menu.clickAt(new Vec2(e.localX,e.localY))){
					createViewsGame();
				}
			}
			/*
			if (e.stageX>920 && e.stageY<580)
			{
				//Pause button
				m_gameStateManager.switchToPause();
			}
			*/
		}
		
		public function touchEnd(e:MouseEvent):void
		{
			if(m_gameStateManager.gameState =="Running")
			{
				if (e.localX<480 && e.currentTarget!=m_joystick && e.stageX<480)
				{

					var vec = new Vec2(e.stageX,e.stageY).sub(m_gestureVec);
					trace("is showing:"+m_gameStateManager.simulation.isMessageShowing());
					if (vec.length > 35 && !m_gameStateManager.simulation.isMessageShowing())
					{
						if(vec.getNormal().x > 0.9 || vec.getNormal().x < -0.9) {
							m_gameStateManager.simulation.player.switchWeapon();
						}
					}
				}
			}
		}
		
		public function onChange(e:AccelerometerEvent)
		{
			/*
			if(m_gameStateManager.gameState == "Running"){
				if (!!m_gameStateManager.simulation)
				m_gameStateManager.simulation.setGyroVec(new Vec2(-e.accelerationX * Options.ACCELEROMETER_SCALER.x ,-0.3+e.accelerationY * Options.ACCELEROMETER_SCALER.y));
			}
			*/
		}

		/**
		* Defines a pressed key as true in an array in order to use it for keyboard input.
		*
		* @param k The keyboard event.
		*/
		public function keyPressed(k:KeyboardEvent):void
		{
			key[k.keyCode]=true;
			trace("key pressed");
			if(m_gameStateManager.gameState == "Running"){
				if (!!m_gameStateManager.simulation)
				{
					m_gameStateManager.simulation.setGyroVec(new Vec2(0,0));
					
					if (key[Keyboard.W])
						m_gameStateManager.simulation.setGyroVec(new Vec2(0,-1));
					if (key[Keyboard.S])
						m_gameStateManager.simulation.setGyroVec(new Vec2(0,1));
					if (key[Keyboard.A])
						m_gameStateManager.simulation.setGyroVec(new Vec2(-1,0));
					if (key[Keyboard.D])
						m_gameStateManager.simulation.setGyroVec(new Vec2(1,0));
					
				}
			}
		}
		
		/**
		* Defines a released key as false in an array in order to use it for keyboard input.
		*
		* @param k The keyboard Event.
		*/
		public function keyReleased(k:KeyboardEvent):void
		{
			key[k.keyCode]=false;
			m_gameStateManager.simulation.setGyroVec(new Vec2(0,0));
		}
		public function destroyViewsGame(){
			m_gameStateManager.removeObserver(m_graphicsManager);
			m_main.removeChild(m_graphicsManager);
			m_main.stage.removeChild(m_joystick);
			m_main.stage.removeChild(m_pause);
			m_main.removeChild(m_graphicsManager.hud);
			m_graphicsManager = null;
		}
		
		public function createViewsGame()
		{
			m_graphicsManager = new GraphicsManager(m_gameStateManager,m_main);
			m_gameStateManager.addObserver(m_graphicsManager);
			m_main.addChildAt(m_graphicsManager,0);
			m_main.stage.addChildAt(m_joystick,1);
			m_main.stage.addChildAt(m_pause,2);
			m_pause.visible = false;
		}
		
		public function get pauseButton():Pause
		{
			return m_pause;
		}
	}
}
