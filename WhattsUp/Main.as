﻿package  {
	
	import flash.display.MovieClip;
	//import com.demonsters.debugger.MonsterDebugger;
    import flash.display.Sprite;
	
	public class Main extends MovieClip {
		
		private var gameStateManager:GameStateManager;
		private var inputManager:InputManager;
		
		public function Main() {
			
			//var stats = new Stats();
			//this.addChild(stats);
			
			//MonsterDebugger.initialize(this);
			gameStateManager = new GameStateManager(this);
			inputManager = new InputManager(this,gameStateManager);
			//Test
		}
	}
	
}
