﻿package  {
	import flash.media.Sound;
	import flash.media.SoundTransform;
	import flash.media.SoundMixer;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.events.Event;
	
	public class SoundManager {
		
		private static var m_transformer:SoundTransform = new SoundTransform;
		private static var m_url:URLRequest;
		private static var m_background:SoundChannel = new SoundChannel;
		private static var m_music:Sound = new Sound();
		private static var m_sounds:Array = new Array;
		private static var m_sounds2:Array = new Array;
		private static var m_volume:Number = 1;
		private static var m_backgroundVol:Number = 0.5; 
		private var m_main:Main;
		private static const MAX_SOUNDS:int = 3;

		
		public function SoundManager(main:Main) {
			m_main = main;
		}
		public static function playSound(sound:Sound, volume:Number, pan:Number):void {
			if (Options.SOUND)
			{
				if(m_sounds.length >= MAX_SOUNDS){
					m_sounds[0].stop();
					m_sounds.splice(0,1);
				}
				var p_channel:SoundChannel;
				m_transformer.volume = volume;
				m_transformer.pan = pan;
				p_channel = (sound.play(0, 1 , m_transformer));
				p_channel.addEventListener(Event.SOUND_COMPLETE,EndSound);
				m_sounds.push(p_channel);
			}
		}
		
		public static function playSound2(sound:Sound, volume:Number, pan:Number):void {
			if (Options.SOUND)
			{
				if(m_sounds2.length >= MAX_SOUNDS){
					m_sounds2[0].stop();
					m_sounds2.splice(0,1);
				}
				var p_channel:SoundChannel;
				m_transformer.volume = volume;
				m_transformer.pan = pan;
				p_channel = (sound.play(0, 1 , m_transformer));
				p_channel.addEventListener(Event.SOUND_COMPLETE,EndSound2);
				m_sounds2.push(p_channel);
			}
		}
		
		public static function playMusic(filename:String):void {
			m_background.stop();
			m_url = new URLRequest(filename);
			m_music = new Sound();
			m_music.addEventListener(Event.COMPLETE, onSoundLoaded);
			m_music.load(m_url);
		}
		private static function EndSound(event:Event):void {
			m_sounds.splice(m_sounds.indexOf(event.target),1);
		}
		
		private static function EndSound2(event:Event):void {
			m_sounds2.splice(m_sounds2.indexOf(event.target),1);
		}
		
		private static function onSoundLoaded(event:Event):void {
			m_transformer.volume = m_backgroundVol;
			m_background = m_music.play(0 , 1 , m_transformer);
			m_background.addEventListener(Event.SOUND_COMPLETE,loopMusic);
        }
		private static function loopMusic(event:Event):void {
			m_background = m_music.play();
		}
		public static function stopMusic():void {
			m_background.stop();
			m_background.removeEventListener(Event.SOUND_COMPLETE,loopMusic);
			m_music.removeEventListener(Event.COMPLETE, onSoundLoaded);
			m_music = null;
		}
		public static function setVolume(vol:Number):void {
			m_volume = vol;
			SoundMixer.soundTransform = new SoundTransform(m_volume);
			
		}
		public static function stopAll():void {
			for(var i = 0;i < m_sounds.length; i++){
				m_sounds[i].stop();
			}
			m_background.stop();
		}

	}
	
}
