﻿package  {
	
	import flash.display.*;
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.geom.Matrix;
	
	public class LightMap {
		
		private var m_lightMap:BitmapData;

		public function LightMap() {
			// constructor code
		}
		
		public function get lightMap():BitmapData
		{
			return m_lightMap;
		}

		public function createLightMap(level:Level)
		{
			var darkField:BitmapData = new BitmapData(level.tileSizeX,level.tileSizeY,false,0x000000);
			var lightField:BitmapData = new BitmapData(level.tileSizeX,level.tileSizeY,false,0xFFFFFF);
			var lamp:BitmapData = ImageLoader.getBitmapDataFromName(ImageLoader.IMAGE_LIGHT);
			
			m_lightMap = new BitmapData(level.worldSizeX*level.tileSizeX,level.worldSizeY*level.tileSizeY,false);
			var tempRect:Rectangle = new Rectangle(0,0,level.tileSizeX,level.tileSizeY);
			var lampRect:Rectangle = new Rectangle(0,0,400,400);
			for (var tileX:int=0;tileX<level.worldSizeX;tileX++)
			{
				for (var tileY:int=0;tileY<level.worldSizeY;tileY++)
				{
						m_lightMap.copyPixels(darkField,tempRect,new Point(tileX*level.tileSizeX,tileY*level.tileSizeY));
				}
			}
			
			//place lights
			for (tileX=0;tileX<level.worldSizeX;tileX++)
			{
				for (tileY=0;tileY<level.worldSizeY;tileY++)
				{
					//testlight
					if (level.functionalData[tileX+(tileY*level.worldSizeX)]==337) 
					{
						var matrix:Matrix = new Matrix();
						matrix.translate(tileX*level.tileSizeX-128,tileY*level.tileSizeY-128);
						//m_lightMap.copyPixels(lamp,lampRect,new Point(tileX*m_level.tileSizeX-128,tileY*m_level.tileSizeY-128));
						m_lightMap.draw(lamp,matrix,null,BlendMode.ADD,null,true);
					}
				}
			}
			
			//override void with black
			for (tileX=0;tileX<level.worldSizeX;tileX++)
			{
				for (tileY=0;tileY<level.worldSizeY;tileY++)
				{
					if (level.getMapdata()[tileX+(tileY*level.worldSizeX)]==0)
						m_lightMap.copyPixels(darkField,tempRect,new Point(tileX*level.tileSizeX,tileY*level.tileSizeY));
				}
			}
		}
	}
	
}
