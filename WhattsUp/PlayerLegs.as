﻿package  {
	
	public class PlayerLegs extends LivingObject {

		public const PLAYER_SIZE:int = 26;
		public const RENDER_SIZE_X:int = 64;
		public const RENDER_SIZE_Y:int = 64;
		private var m_player:Player;
		private var m_walkcycle:int = 0;
		
		public function PlayerLegs(player:Player,simulation:Simulation) {
			m_player = player;
			super(m_player.position,PLAYER_SIZE,ImageLoader.IMAGE_PLAYER_LEGS,simulation,new Vec2(RENDER_SIZE_X,RENDER_SIZE_Y));
		}

		public function step():void
		{
			if (m_player.velocity)
			{
				if (m_player.velocity.length>0.18)
					m_walkcycle++;
				if (m_walkcycle>7) m_walkcycle = 0;
				
				m_frame = Math.floor(m_walkcycle/2);
			}
		}
	}
	
}
