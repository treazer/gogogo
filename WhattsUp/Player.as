﻿package  {
	
	public class Player extends LivingObject {

		private var weapon1:Weapon;
		private var weapon2:Weapon;
		public const PLAYER_SIZE:int = 26;
		public const RENDER_SIZE_X:int = 64;
		public const RENDER_SIZE_Y:int = 64;
		private var m_playerLegs:PlayerLegs;
		
		public function Player(pos:Vec2,simulation:Simulation) {
			super(pos,PLAYER_SIZE,ImageLoader.IMAGE_PLAYER,simulation,new Vec2(RENDER_SIZE_X,RENDER_SIZE_Y))
			weapon1 = new Weapon(0);
			weapon2 = new Weapon(2);
			m_currentWeapon = weapon2;
			m_velocity = new Vec2(0,0);
		}
		
		public function step():void
		{
			m_velocity = m_simulation.gyroVec;
			move(m_velocity.scale(20));
			resolveCollisionEnemies();
			weapon1.step();
			weapon2.step();
			
			if (m_playerLegs)
			{
				m_playerLegs.setDirection(m_velocity);
				m_playerLegs.setPosition(m_position);
			}
			
			if (m_health<0)
			{
				m_simulation.endGame();
			}
			if (m_timeSinceLastShot>20)
				m_direction = m_simulation.gyroVec;
		}
		public function switchWeapon():void {
			if(m_currentWeapon == weapon1)
				{m_currentWeapon = weapon2}
			else if(m_currentWeapon == weapon2)
				{m_currentWeapon = weapon1}
		}
		
		public function resolveCollisionEnemies():void
		{
			for each (var enemy:GameObject in m_simulation.gameObjects)
			{
				if (enemy is Enemy)
				{
					if (enemy.size+this.size>enemy.distanceToPoint(this.position))
					{
						//calc size of the applied movement
						var scale:Number = (enemy.size +this.size-enemy.distanceToPoint(this.position))/enemy.size + this.size;
						var solveDirection:Vec2 = enemy.position.sub(this.position).normalize();
						
						this.m_position = this.position.sub(solveDirection.scale(0.1*scale));
					}
				}
			}
		}
		
		public function setPlayerLegs(value:PlayerLegs)
		{
			m_playerLegs = value;
		}

	}
	
}
