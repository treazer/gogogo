﻿package  {
	
	public class TriggerShake extends Trigger {

		private var m_intensity:Number;
		private var m_duration:int;
		private var m_fired:Boolean = false;
		
		public function TriggerShake(simulation:Simulation,position:Vec2,radius,intensity,duration) {
			super(simulation,position,radius,false,-1);
			m_intensity = intensity;
			m_duration = duration;
		}
		
		override public function step():void
		{
			if (!m_fired)
			{
				if (m_simulation.player.distanceToPoint(m_position) < m_radius)
				{
					m_simulation.setScreenShake(m_duration,m_intensity);
					m_fired = true;
				}
			}
		}

	}
	
}
