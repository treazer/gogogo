﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.TouchEvent;
	import flash.sensors.Accelerometer;
	import flash.events.AccelerometerEvent;
	import flash.ui.Multitouch;
    import flash.ui.MultitouchInputMode;
	import flash.events.MouseEvent;
	
	
	public class Joystick extends MovieClip {
		
		private var m_direction:Vec2;
		private var touchinggg:Boolean;

		public function Joystick() {
			m_direction = new Vec2(0,0);
			
			this.addEventListener(MouseEvent.MOUSE_DOWN,touchStart);
			this.addEventListener(MouseEvent.MOUSE_UP,touchEnd);
			this.addEventListener(MouseEvent.MOUSE_MOVE,touching);

			this.x=800;
			this.y=480;
		}

		public function touchStart(e:MouseEvent):void
		{
			//this.x=this.x+e.localX;
			//this.y=this.y+e.localY;
			m_direction = new Vec2(0,0);
			this.x=800;
			this.y=480;
			touchinggg=true;
		}
		
		public function touchEnd(e:MouseEvent):void
		{
			m_direction = new Vec2(0,0);
			this.x=800;
			this.y=480;
			touchinggg=false;
		}
		
		public function touching(e:MouseEvent):void
		{
			if (touchinggg)
				m_direction = new Vec2(e.localX,e.localY).normalize();
		}
		
		public function get direction():Vec2
		{
			return m_direction;
		}
	}	
}
