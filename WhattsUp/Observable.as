﻿package {

	public class Observable {
		public var observers:Array;

		public function Observable() {
			this.observers = new Array();
		}

		public function addObserver(interestedParty:Object):void {
			this.observers.push(interestedParty);
		}

		public function removeObserver(desinterestedParty:Object):void {
			for (var i:int = 0; i < this.observers.length; i++) {
				if (this.observers[i] == desinterestedParty) {
					this.observers.splice(i,1);
					break;
				}
			}
		}

		public function notifyObservers():void {
			for (var i:int = 0; i < this.observers.length; i++) {
				this.observers[i].update(this,null);
			}
		}

	}

}