﻿package  {
	
	import flash.display.*;
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.filters.*;
	import flash.geom.Matrix;
	
	public class AOMap {

		public static const SHADOW_DIRECTION_X:int = 13;
		public static const SHADOW_DIRECTION_Y:int = 13;
		public static const AMBIENT_SIZE:int = 13;
		private var m_ambientMap:BitmapData;
		
		public function AOMap() {
			// constructor code
		}
		
		public function get ambientMap():BitmapData
		{
			return m_ambientMap;
		}
		
		public function createAmbientMap(level:Level)
		{
			var darkField:BitmapData = new BitmapData(level.tileSizeX,level.tileSizeY,false,0x333333);
			var lightField:BitmapData = new BitmapData(level.tileSizeX,level.tileSizeY,false,0xFFFFFF);
			var shadowField:BitmapData = new BitmapData(level.tileSizeX,level.tileSizeY,false,0x666666);
			var tempRect:Rectangle = new Rectangle(0,0,level.tileSizeX,level.tileSizeY);
			var shadowRect:Rectangle = new Rectangle(0,0,level.tileSizeX+SHADOW_DIRECTION_X,level.tileSizeY+SHADOW_DIRECTION_Y);
			
			m_ambientMap = new BitmapData(level.worldSizeX*level.tileSizeX,level.worldSizeY*level.tileSizeY,false,0xFFFFFF);
			
			for (var tileX=0;tileX<level.worldSizeX;tileX++)
			{
				for (var tileY=0;tileY<level.worldSizeY;tileY++)
				{
					if (level.isFieldSolid(new Vec2(tileX,tileY)))
						m_ambientMap.copyPixels(darkField,tempRect,new Point(tileX*level.tileSizeX,tileY*level.tileSizeY));
					else
						m_ambientMap.copyPixels(lightField,tempRect,new Point(tileX*level.tileSizeX,tileY*level.tileSizeY));
				}
			}
			m_ambientMap = blurBitmapData(m_ambientMap,AMBIENT_SIZE);
			for (tileX=0;tileX<level.worldSizeX;tileX++)
			{
				for (tileY=0;tileY<level.worldSizeY;tileY++)
				{
					//shadows 50,36,38
					if (level.getMapdata()[tileX+(tileY*level.worldSizeX)]==50 ||
						level.getMapdata()[tileX+(tileY*level.worldSizeX)]==38 ||
						level.getMapdata()[tileX+(tileY*level.worldSizeX)]==36)
						{
							for (var sX:int = 0;sX<SHADOW_DIRECTION_X;sX+=2)
							{
								for (var sY:int = 0;sY<SHADOW_DIRECTION_X;sY+=2)
								{
									m_ambientMap.copyPixels(shadowField,tempRect,new Point((tileX*level.tileSizeX)+SHADOW_DIRECTION_X-sX,(tileY*level.tileSizeY)+SHADOW_DIRECTION_Y-sY));
								}
							}
						}
					}
			}
			m_ambientMap = blurBitmapData(m_ambientMap,AMBIENT_SIZE*0.7);
			
			for (tileX=0;tileX<level.worldSizeX;tileX++)
			{
				for (tileY=0;tileY<level.worldSizeY;tileY++)
				{
					if (level.isFieldSolid(new Vec2(tileX,tileY)))
						m_ambientMap.copyPixels(lightField,tempRect,new Point(tileX*level.tileSizeX,tileY*level.tileSizeY));
				}
			}
		}
		
		public function blurBitmapData(bitmapdata:BitmapData,blurvalue:Number):BitmapData
		{
			var result:BitmapData = bitmapdata.clone();
			var blur:BlurFilter = new BlurFilter();
			blur.blurX = blurvalue; 
			blur.blurY = blurvalue; 
			blur.quality = BitmapFilterQuality.MEDIUM;
			result.applyFilter(result,new
			Rectangle(0,0,bitmapdata.width,bitmapdata.height),new Point(0,0),blur);
			return result;
		}
	}
	
}
