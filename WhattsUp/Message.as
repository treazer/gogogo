﻿package  {
	
	public class Message {
		
		private var m_content:String;
		private var m_id:int;
		private var m_showing:Boolean = false;
		private var m_done:Boolean = false;

		public function Message(content:String,id:int) {
			m_content = content;
			m_id = id;
		}

		public function get content():String
		{
			return m_content;
		}
		
		public function setContent(value:String)
		{
			m_content = value;
		}
		
		public function showMessage()
		{
			if (!m_done)
			{
				m_showing = true;
			}
			
		}
		
		public function done()
		{
			m_done = true;
			m_showing = false;
		}
		
		public function get isShowing()
		{
			return m_showing;
		}
		
		public function get isDone()
		{
			if (m_showing && !m_done)
				return true;
			return false;
		}
		
		public function get id():int
		{
			return m_id;
		}
	}
	
}
