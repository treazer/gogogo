﻿package  {
	
	import flash.display.MovieClip;
	import flash.ui.MultitouchInputMode;
	import flash.events.MouseEvent;
	import flash.ui.Multitouch;
	import flash.events.TouchEvent;
	
	public class Pause extends MovieClip {
		
		private var m_isPresst:Boolean = false;
		
		public function Pause() {
			this.addEventListener(MouseEvent.MOUSE_DOWN,touchStart);
			this.addEventListener(MouseEvent.MOUSE_UP,touchEnd);
			this.x = 960-64-10;
			this.y = 10;
		}
		public function touchStart(e:MouseEvent):void
		{
			m_isPresst = true;
		}
		
		public function touchEnd(e:MouseEvent):void
		{
			m_isPresst = false;
		}
		public function get isPresst():Boolean{
			return m_isPresst;
		}
		public function set isPresst(isPresst:Boolean){
			m_isPresst = isPresst;
		}
		
	}
	
}
