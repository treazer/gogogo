﻿package  {
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.geom.Rectangle;
	import flash.geom.Matrix;
	
	public class PauseMenu extends MovieClip{
		
		private var menuObjects:Array = new Array;
		private var m_gameStateManager:GameStateManager;

		public function PauseMenu(gameStateManager:GameStateManager) {
			m_gameStateManager = gameStateManager;
			gameStateManager.main.stage.addChild(this);
			menuObjects = new Array;
			createPause();
		}
		public function step():void{
			draw();
		}
		public function draw():void{
			if(ImageLoader.isDoneLoading()){
				graphics.clear();
				for(var i = 0; i < menuObjects.length; i++){
					var matrix:Matrix = new Matrix();
					matrix.translate(menuObjects[i].pos.x,menuObjects[i].pos.y);
					this.graphics.beginBitmapFill(ImageLoader.getBitmapDataFromName(menuObjects[i].image), matrix);
					this.graphics.drawRect(menuObjects[i].pos.x,menuObjects[i].pos.y,menuObjects[i].size.x,menuObjects[i].size.y);
					this.graphics.endFill();
				}
			}
		}
		private function createPause():void{
			menuObjects.push(new MenuImage(new Vec2(0,0),ImageLoader.STANDART_BACKGROUND_SIZE,"PauseBackground.png","Background"));
			menuObjects.push(new MenuImage(new Vec2(480 - 157 ,400),new Vec2(315,63),"Quit.png","Main"));
			menuObjects.push(new MenuImage(new Vec2(480 - 157 ,300),new Vec2(315,63),"Resume.png","Resume"));
		}
		public function remove():void{
			m_gameStateManager.main.stage.removeChild(this);
			deletMenuObjects();
		}
		private function deletMenuObjects(){
			menuObjects.length = 0;
		}

	}
	
}
