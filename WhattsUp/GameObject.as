﻿package  {
	import flash.display.BitmapData;
	
	public class GameObject {
		
		protected var m_position:Vec2;
		protected var m_velocity:Vec2;
		protected var m_image:String = ImageLoader.IMAGE_DEFAULT;
		protected var m_size:int;
		protected var m_frame:int;
		protected var m_simulation:Simulation;
		protected var m_renderSize:Vec2;
		protected var m_direction:Vec2;

		public function GameObject(pos:Vec2,size:int, image:String, simulation:Simulation, renderSize:Vec2){
			m_position = pos;
			m_size = size;
			m_image = image;
			m_simulation = simulation;
			m_renderSize = renderSize;
			m_frame = 0;
			m_direction = new Vec2(0,0);
			m_velocity = new Vec2(0,0);
			
		}
		
		public function distanceToPoint(value:Vec2):Number
		{
			var diff:Vec2 = m_position.sub(value);
			return diff.length;
		}
		public function get frame():int{
			return m_frame;
		}
		public function get image():String{
			return m_image;
		}
		
		public function get position():Vec2
		{
			return m_position;
		}
		
		public function get velocity():Vec2
		{
			return m_velocity;
		}
		
		public function get size():int
		{
			return m_size;
		}
		
		public function get renderSize():Vec2
		{
			return m_renderSize;
		}
		
		public function setRenderSize(value:Vec2):void
		{
			m_renderSize = value;
		}
		
		public function setPosition(value:Vec2):void
		{
			m_position = value;
		}
		
		public function setVelocity(value:Vec2):void
		{
			m_velocity = value;
		}
		
		public function get direction():Vec2
		{
			return m_direction;
		}
		
		public function setDirection(value:Vec2):void
		{
			m_direction = value;
		}

	}
	
}
