﻿package  {
	
	public class Trigger {
		
		protected var m_position:Vec2;
		protected var m_radius:int;
		private var m_spawns:Vector.<Spawn>;
		protected var m_simulation:Simulation;
		private var m_gameEndTrigger:Boolean;
		protected var m_delay:int; //not implemented yet
		protected var m_id:int;
		protected var m_triggered:Boolean; //not implemented yet
		
		public function Trigger(simulation:Simulation,position:Vec2,radius:int,gameEndTrigger:Boolean=false,id:int=-1) {
			//position is given as tile-coordinate
			m_position = new Vec2(position.x,position.y);
			m_position = position;
			m_radius = radius;
			m_simulation = simulation;
			m_spawns = new Vector.<Spawn>();
			m_gameEndTrigger = gameEndTrigger;
			m_id = id;
		}

		public function step():void
		{
			if (m_spawns.length>0 || m_gameEndTrigger)
			{
				if (m_simulation.player.distanceToPoint(m_position) < m_radius)
				{
					if (m_gameEndTrigger)
						notifyGameEnds();
					else
						notifySpawns();
				}
			}
		}
		
		public function addSpawn(value:Spawn):void
		{
			m_spawns.push(value);
		}
		
		public function notifySpawns():void
		{
			for each(var spawn:Spawn in m_spawns)
			{
				spawn.trigger();
			}
			m_spawns = new Vector.<Spawn>();
		}
		
		public function notifyGameEnds():void
		{
			m_simulation.endGame();
		}
		
		public function get id():int
		{
			return m_id;
		}
	}
	
}
