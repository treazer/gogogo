﻿package  {
	
	public class LivingObject extends GameObject {

		protected var m_health:int = 0;
		protected var m_maxHealth:int = 0;
		protected var maxSpeed:Number = 8;
		protected var state:String = "";
		protected var m_currentWeapon:Weapon;
		protected var m_animation:int = 0;
		protected var m_timeSinceLastShot:int = 100;
		
		private var corner:Vec2;
		private var solver:Vec2;
		
		public function LivingObject(pos:Vec2,size:int,image,simulation:Simulation,renderSize:Vec2) {
			super(pos,size,image,simulation,renderSize);
			m_maxHealth = 100;
			m_health = m_maxHealth;
		}
		
		public function move(value:Vec2):void {
			m_timeSinceLastShot++;
			m_velocity = value;
			//max speed limitation
			if (m_velocity.length>maxSpeed)
				m_velocity = m_velocity.normalize().scale(maxSpeed);
			
			
			this.m_position = this.position.add(m_velocity);
			checkCollision();
		}
		
		public function getCurrentField():Vec2
		{
			var x:int = Math.floor(m_position.x/m_simulation.level.tileSizeX);
			var y:int = Math.floor(m_position.y/m_simulation.level.tileSizeY);
			return new Vec2(x,y);
		}
		
		public function checkCollision()
		{
			var currentField:Vec2 = getCurrentField();
			var checkField:Vec2;
			
			
				
			//top
			checkField = new Vec2(currentField.x,currentField.y-1);
			if (m_simulation.level.isFieldSolid(checkField))
			{
				if (m_position.y-m_size<(checkField.y*m_simulation.level.tileSizeY)+m_simulation.level.tileSizeY)
				{
					m_position.setY((checkField.y*m_simulation.level.tileSizeY)+m_simulation.level.tileSizeY+m_size);
					blockMovementUp();
				}
			}
			
			//down
			checkField = new Vec2(currentField.x,currentField.y+1);
			if (m_simulation.level.isFieldSolid(checkField))
			{
				if (m_position.y+m_size>(checkField.y*m_simulation.level.tileSizeY))
				{
					m_position.setY((checkField.y*m_simulation.level.tileSizeY)-m_size);
					blockMovementDown();
				}
			}
			
			//left
			checkField = new Vec2(currentField.x-1,currentField.y);
			if (m_simulation.level.isFieldSolid(checkField))
			{
				if (m_position.x-m_size<(checkField.x*m_simulation.level.tileSizeX)+m_simulation.level.tileSizeX)
				{
					m_position.setX((checkField.x*m_simulation.level.tileSizeX)+m_simulation.level.tileSizeX+m_size);
					blockMovementLeft();
				}
			}
			
			//right
			checkField = new Vec2(currentField.x+1,currentField.y);
			if (m_simulation.level.isFieldSolid(checkField))
			{
				if (m_position.x+m_size>(checkField.x*m_simulation.level.tileSizeX))
				{
					m_position.setX((checkField.x*m_simulation.level.tileSizeX)-m_size);
					blockMovementRight();
				}
			}
				
			//top left
			checkField = new Vec2(currentField.x-1,currentField.y-1);
			if (m_simulation.level.isFieldSolid(checkField))
			{
				corner = new Vec2((currentField.x*m_simulation.level.tileSizeX),(currentField.y*m_simulation.level.tileSizeY));
				if (m_position.sub(corner).length<m_size)
				{
					solver = m_position.sub(corner).normalize().scale(m_size-m_position.sub(corner).length);
					m_position = m_position.add(solver);
				}
			}
			
			//top right
			checkField = new Vec2(currentField.x+1,currentField.y-1);
			if (m_simulation.level.isFieldSolid(checkField))
			{
				corner = new Vec2((currentField.x*m_simulation.level.tileSizeX)+m_simulation.level.tileSizeX,(currentField.y*m_simulation.level.tileSizeY));
				if (m_position.sub(corner).length<m_size)
				{
					solver = m_position.sub(corner).normalize().scale(m_size-m_position.sub(corner).length);
					m_position = m_position.add(solver);
				}
			}
			
			//down left
			checkField = new Vec2(currentField.x-1,currentField.y+1);
			if (m_simulation.level.isFieldSolid(checkField))
			{
				corner = new Vec2((currentField.x*m_simulation.level.tileSizeX),(currentField.y*m_simulation.level.tileSizeY)+m_simulation.level.tileSizeY);
				if (m_position.sub(corner).length<m_size)
				{
					solver = m_position.sub(corner).normalize().scale(m_size-m_position.sub(corner).length);
					m_position = m_position.add(solver);
				}
			}
			
			//down right
			checkField = new Vec2(currentField.x+1,currentField.y+1);
			if (m_simulation.level.isFieldSolid(checkField))
			{
				corner = new Vec2((currentField.x*m_simulation.level.tileSizeX)+m_simulation.level.tileSizeX,(currentField.y*m_simulation.level.tileSizeY)+m_simulation.level.tileSizeY);
				if (m_position.sub(corner).length<m_size)
				{
					solver = m_position.sub(corner).normalize().scale(m_size-m_position.sub(corner).length);
					m_position = m_position.add(solver);
				}
			}
		}
		
		public function blockMovementUp()
		{
			if (m_velocity.y<0)
				m_velocity = new Vec2(m_velocity.x,0);
		}
		
		public function blockMovementDown()
		{
			if (m_velocity.y>0)
				m_velocity = new Vec2(m_velocity.x,0);
		}
		
		public function blockMovementLeft()
		{
			if (m_velocity.x<0)
				m_velocity = new Vec2(0,m_velocity.y);
		}
		
		public function blockMovementRight()
		{
			if (m_velocity.x>0)
				m_velocity = new Vec2(0,m_velocity.y);
		}
		
		public function shoot(aim:Vec2,friendly:Boolean):void {
			
			m_currentWeapon.checkForShoot();
			if(m_currentWeapon.ready == true){
				m_timeSinceLastShot=0;
				m_direction = aim;
				m_currentWeapon.startCooldown();
				aim = aim.add(new Vec2(Math.random()*m_currentWeapon.straying,Math.random()*m_currentWeapon.straying));
				var bullet:Bullet = new Bullet(m_position,aim,1,m_simulation,m_currentWeapon.bulletSize,m_currentWeapon.bulletRenderSizeX,m_currentWeapon.bulletRenderSizeY,m_currentWeapon.bullet,m_currentWeapon.damage,m_currentWeapon.sizeIncrease,friendly,m_currentWeapon.bulletLife);
				if (friendly)
				{
					if (m_currentWeapon.name=="Fry Bye")
						SoundManager.playSound(new Player_shot_2(),1,0);
					if (m_currentWeapon.name=="FS80 Blister")
						SoundManager.playSound(new Player_shot_1(),1,0);
					if (m_currentWeapon.name=="A.Y.S.")
						SoundManager.playSound(new Player_shot_1(),1,0);
				}
				if (m_currentWeapon.name=="Spitter")
					SoundManager.playSound2(new Enemy_shot_1(),1,0);
				
				m_simulation.gameObjects.push(bullet);
			}
		}
		public function hit(damage:int){
			m_health -= damage;
		}
		
		public function get maxHealth():int {
			return m_maxHealth;
		}
		
		public function get health():int {
			return m_health;
		}
		public function get currentWeapon():Weapon
		{
			return m_currentWeapon;
		}
		
		public function get animation():int
		{
			return m_animation;
		}
	
	}
	
	
}
