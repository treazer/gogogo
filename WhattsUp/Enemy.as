﻿package  {
	
	public class Enemy extends LivingObject {
		
		private const SPEED = 4;
		private const DISTANCE_FOR_ATTACK = 175;	
		private const DISTANCE_FOR_APPROACH = 140;
		private const DISTANCE_FOR_SPOTTING = 430;
		private const ENEMY_SIZE = 32;
		private const ENEMY_HEALTH = 35;
		public const RENDER_SIZE_X:int = 64;
		public const RENDER_SIZE_Y:int = 64;
		
		public static const STATE_STAYING:String = "staying";
		public static const STATE_CHASING:String = "chasing";
		public static const STATE_SHOOTING:String = "shooting";
		
		private var m_state:String = STATE_STAYING;
		private var m_spottedPlayer:Boolean = false;
		
		public function Enemy(pos:Vec2,simulation:Simulation) {
			super(pos,ENEMY_SIZE,ImageLoader.IMAGE_ENEMY_1,simulation,new Vec2(RENDER_SIZE_X,RENDER_SIZE_Y))
			this.m_position = pos;
			m_health = ENEMY_HEALTH;
			m_currentWeapon = new Weapon(3);
			m_velocity = new Vec2(Math.random(),Math.random());
			m_frame = 10;
		}
		
		public function step():Boolean
		{
			m_currentWeapon.step();
			resolveCollision();
			move(this.velocity);
			m_direction = m_velocity;
			
			//determine state
			if (m_spottedPlayer)
			{
				if (m_simulation.player.distanceToPoint(this.position)>this.DISTANCE_FOR_APPROACH)
					m_state = STATE_CHASING;
				
				if (m_simulation.player.distanceToPoint(m_position)<this.DISTANCE_FOR_ATTACK)
					m_state = STATE_SHOOTING;
			}
			else
			{
				if (m_simulation.player.distanceToPoint(this.position)<this.DISTANCE_FOR_SPOTTING)
				{
					m_state = STATE_CHASING;
					m_spottedPlayer = true;
				}
			}
			//execute state
			executeBehaviour();
			
			if (health<=0) {
				SoundManager.playSound2(new EnemyDying_1(),1,0);
				return true;
			}
			
			return false;
		}
		
		public function executeBehaviour():void
		{
			switch(m_state){
				case STATE_STAYING:
					m_velocity = m_velocity.scale(0);
				break;
				case STATE_CHASING:
					moveTowardsPlayer();
					if (m_velocity.length>0.14)
					{
						m_frame++;
					}
					if (m_frame>19)
						m_frame = 10;
				break;
				case STATE_SHOOTING:
					this.shoot(m_simulation.player.position.sub(m_position).normalize(),false);
					m_frame++;
					
					if (m_frame>9)
						m_frame = 0;
				break;
			}
		}
		
		public function moveTowardsPlayer():void
		{
			this.m_velocity = m_simulation.player.position.sub(this.position).normalize().scale(this.SPEED);
		}
		
		public function resolveCollision():void
		{
			for each (var enemy:GameObject in m_simulation.gameObjects)
			{
				if (enemy is Enemy)
				{
					if (enemy!=this && enemy.size +this.size>enemy.distanceToPoint(this.position))
					{
						//calc size of the applied movement
						var scale:Number = (enemy.size +this.size-enemy.distanceToPoint(this.position))/enemy.size + this.size;
						var solveDirection:Vec2 = enemy.position.sub(this.position).normalize();
						
						enemy.setPosition(enemy.position.add(solveDirection.scale(0.01*scale)));
						this.m_position = this.position.sub(solveDirection.scale(0.01*scale));
					}
				}
			}

		}
		
	}
}
