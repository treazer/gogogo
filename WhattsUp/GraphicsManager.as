﻿package  {
	
	import flash.display.MovieClip;
	import flash.events.*;
	import flash.display.Stage;
	import flash.text.*; 
	import flash.display.*;
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.geom.Matrix;
	import flash.system.*;
	import flash.filters.*;
	
	import flash.text.TextField;
	import flash.text.TextFormat;  
    import flash.text.TextFieldAutoSize; 
	import flash.geom.ColorTransform;
	
	
	public class GraphicsManager extends MovieClip implements IObserver {
		
		private var m_gameStateManager:GameStateManager;
		private var m_main:Main;
		private var m_cameraOffset:Vec2;
		private var m_renderMap:BitmapData;
		private var m_lightMap:BitmapData;
		private var m_level:Level;
		private var m_hud:Hud;
		private var m_createdMapImage:Boolean = false;
		private var m_simulation:Simulation;
		private var m_offsetMatrix:Matrix;
		private var m_textMessage:TextField;
		private var m_textFormat:TextFormat;
		private var m_messageMatrix:Matrix;
		
		private var render_matrix:Matrix;
		private var render_imageData:BitmapData;
		private var render_rotation:Number;
		
		private var m_shadowMap:AOMap;
		
		private var m_fadeBitmap:BitmapData;
		private var m_fade:int = 0;
		private var m_fading:Number;
		private var m_endingGame:Boolean=false; //needs to be reseted for another level
		
		public function GraphicsManager(gameStateManager:GameStateManager,main:Main) {
			
			m_gameStateManager = gameStateManager;
			m_level = m_gameStateManager.simulation.level;
			m_simulation = m_gameStateManager.simulation;
			m_main = main;
			m_cameraOffset = new Vec2(0,0);
			
			m_hud = new Hud(this);
			m_main.addChild(m_hud);
			m_offsetMatrix = new Matrix();
			m_textFormat = new TextFormat();
			m_textFormat.font = "Tahoma";
			m_textFormat.size = 36;
			m_textFormat.bold = true;
			m_textFormat.color = 0xF5F9FF;
			m_textMessage = new TextField();
			this.addChild(m_textMessage);
			
			m_messageMatrix = new Matrix();
			m_messageMatrix.translate(40,380);
		}
		
		public function update(o: Observable, arg: Object):void
		{
			if (m_simulation.isEnding2 && !m_endingGame)
			{
				m_endingGame = true;
				fadeOut();
			}
			
			m_cameraOffset = m_simulation.player.position.add(new Vec2(-480,-400));
			
			if (m_cameraOffset.x<0) m_cameraOffset.setX(0);
			if (m_cameraOffset.y<0) m_cameraOffset.setY(0);
			m_offsetMatrix = new Matrix();
			m_offsetMatrix.translate(0-m_cameraOffset.x,0-m_cameraOffset.y);
			
			graphics.clear();
			if (ImageLoader.isDoneLoading() && !m_createdMapImage && m_level.hasLoaded)
			{
				createMapImage();
				fadeIn();
			}
			if (m_createdMapImage)
			{
				renderMap();
				renderGameObjects();
				renderHud();
				displayMessages();
				
				
				if (m_textMessage.visible)
				{
					this.graphics.endFill();
					this.graphics.beginBitmapFill(ImageLoader.getBitmapDataFromName(ImageLoader.IMAGE_MESSAGE),m_messageMatrix);
					this.graphics.drawRect(40,380,880,200);
				}
				else
				{
					drawJoystick();
				}
			}
			else
			{
				this.graphics.beginBitmapFill(ImageLoader.getBitmapDataFromName(ImageLoader.IMAGE_LOADING));
				this.graphics.drawRect(0,0,960,640);
			}
			//m_cameraOffset = m_cameraOffset.add(new Vec2(2,2));
			    
			screenShake();
			fade();
		}
		
		public function drawJoystick()
		{
			this.graphics.endFill();
			this.graphics.lineStyle(1,0xFFFFFF,0.6);
			this.graphics.beginFill(0x000000,0.15);
			this.graphics.drawCircle(800,480,140);
		}
		
		public function displayMessages()
		{
			var renderingStuff:Boolean = false;
			for each (var msg:Message in m_simulation.messages)
			{
				if (msg.isShowing)
				{
					m_textMessage.x=60;
					m_textMessage.y=400;
					m_textMessage.width=840;
					m_textMessage.htmlText = msg.content;
					m_textMessage.visible = true;
					m_textMessage.setTextFormat(m_textFormat);
					m_textMessage.multiline = true;
					m_textMessage.wordWrap = true;
					renderingStuff = true;
				}
				if (renderingStuff==false)
					m_textMessage.visible=false;
			}
		}
		
		public function renderGameObjects()
		{
			for each (var gameObject:GameObject in m_simulation.gameObjects)
			{
				render_rotation = 0;
				render_matrix = new Matrix();
				
					render_rotation = (Math.atan2(gameObject.direction.normalize().y, gameObject.direction.normalize().x))-1.5;
					if (!render_rotation) render_rotation = 0;

				render_imageData = getSprite(gameObject.frame,0,ImageLoader.getBitmapDataFromName(gameObject.image),gameObject.renderSize.x,gameObject.renderSize.y,render_rotation);
				render_matrix.translate(gameObject.position.x-m_cameraOffset.x-(gameObject.renderSize.x/2),gameObject.position.y-m_cameraOffset.y-(gameObject.renderSize.y/2));
				
				
				
				if (gameObject is Bullet)
				{
					render_imageData = getSprite(
					gameObject.frame,0,ImageLoader.getBitmapDataFromName(gameObject.image),
					64,64,render_rotation);
					
					var resizeMatrix:Matrix = new Matrix();
					resizeMatrix.scale(gameObject.renderSize.x/64,gameObject.renderSize.y/64);
					
					var tempBitmap:BitmapData = new BitmapData(gameObject.renderSize.x,gameObject.renderSize.y,true,0x00000000);
					tempBitmap.draw(render_imageData,resizeMatrix);
					render_imageData = tempBitmap;
					render_matrix = new Matrix();
					render_matrix.translate(
					gameObject.position.x-m_cameraOffset.x-(render_imageData.width/2),
					gameObject.position.y-m_cameraOffset.y-(render_imageData.height/2));
				
					
					//drawing
					this.graphics.beginBitmapFill(render_imageData,render_matrix,true);
					this.graphics.drawRect(gameObject.position.x-m_cameraOffset.x-(gameObject.renderSize.x/2),gameObject.position.y-m_cameraOffset.y-(gameObject.renderSize.y/2),gameObject.renderSize.x,gameObject.renderSize.y);
					///////
					//this.graphics.beginFill(0x00FF00,1);
					//this.graphics.drawRect(gameObject.position.x-m_cameraOffset.x-(gameObject.renderSize.x/2),gameObject.position.y-m_cameraOffset.y-(gameObject.renderSize.y/2),5,5);
					
					//this.graphics.endFill();
					//this.graphics.lineStyle(1,0x00FF00,1);
					//this.graphics.drawRect(gameObject.position.x-m_cameraOffset.x-(gameObject.renderSize.x/2),gameObject.position.y-m_cameraOffset.y-(gameObject.renderSize.y/2),gameObject.renderSize.x,gameObject.renderSize.y);
					
					
					///////
				}
				else
				{
				
					this.graphics.beginBitmapFill(render_imageData,render_matrix,true);
					

					if (gameObject.image)
					{
							this.graphics.drawRect(gameObject.position.x-m_cameraOffset.x-(gameObject.renderSize.x/2),gameObject.position.y-m_cameraOffset.y-(gameObject.renderSize.y/2),gameObject.renderSize.x,gameObject.renderSize.y);
					}
				}
			}
		}
		
		public function renderMap():void
		{
			
			this.graphics.beginBitmapFill(m_renderMap,m_offsetMatrix,false,false);
			//this.graphics.drawRect(m_cameraOffset.x,m_cameraOffset.y,(m_level.worldSizeX*m_level.tileSizeX)+m_cameraOffset.x,(m_level.worldSizeY*m_level.tileSizeY)+m_cameraOffset.y);
			this.graphics.drawRect(0,0,960,640);
			this.graphics.endFill();
		}
		
		public function renderHud():void {

			m_hud.step(new Vec2(0,0), m_simulation.player);

		}
		
		public function createMapImage()
		{			
			var diffuseMap:DiffuseMap = new DiffuseMap();
			diffuseMap.createDiffuseMap(m_level);
			
				m_renderMap = diffuseMap.diffuseMap;
			
			var aoMap:AOMap = new AOMap();
			aoMap.createAmbientMap(m_level);
			
				m_renderMap.draw(aoMap.ambientMap,null,null,BlendMode.MULTIPLY,null,true);
				aoMap = null;
			
			var lightMap:LightMap = new LightMap();
			lightMap.createLightMap(m_level);

				m_renderMap.draw(lightMap.lightMap,null,null,BlendMode.MULTIPLY,null,true);
				lightMap = null;
			
			System.gc();
			
			m_createdMapImage=true;
		}
		
		public function getSprite(frame:int,animation:int,spritesheet:BitmapData,sizeX:int,sizeY:int,rotation:Number):BitmapData
		{
			var tileX:int = frame;
			var tileY:int = animation;
			var matrix:Matrix = new Matrix();
			matrix.tx-=sizeX/2;
			matrix.ty-=sizeY/2;
			matrix.rotate(rotation);
			matrix.tx+=sizeX/2;
			matrix.ty+=sizeY/2;
			
			var tempData:BitmapData = new BitmapData(sizeX,sizeY,true);
			var tempData2:BitmapData = new BitmapData(sizeX,sizeY,true,0);
			var tempRect:Rectangle = new Rectangle(tileX*sizeX,tileY*sizeY,sizeX,sizeY);
			tempData.copyPixels(spritesheet,tempRect,new Point(0,0));
			tempData2.draw(tempData,matrix);
			return tempData2;
		}
		
		public function get gameStateManager()
		{
			return m_gameStateManager;

		}
		public function get hud(){
			return m_hud;
		}
		
		public function screenShake()
		{
			if (m_simulation.screenShakeTimer>0)
			{
				m_main.x = (Math.random()*m_simulation.screenShakeIntensity)-m_simulation.screenShakeIntensity/2;
				m_main.y = (Math.random()*m_simulation.screenShakeIntensity)-m_simulation.screenShakeIntensity/2;
			}
			else
			{
				m_main.x=0;
				m_main.y=0;
			}
		}
		
		public function fadeIn()
		{
			m_fade = 1;
			m_fading = 120;
			m_fadeBitmap = new BitmapData(1,1,true,0x99000000);
		}
		
		public function fadeOut()
		{
			m_fade = 2;
			m_fading = 160;
			m_fadeBitmap = new BitmapData(1,1,true,0x02000000);
		}
		
		public function fade()
		{
			m_fading--;
			if (m_fade==1)
			{
				m_fadeBitmap.colorTransform(new Rectangle(0,0,960,640),new ColorTransform(1,1,1,0.98));
				this.graphics.beginBitmapFill(m_fadeBitmap);
				this.graphics.drawRect(0, 0, 960, 640);
				this.graphics.endFill();
				if (m_fading<0) m_fade = 0;
				
			}
			if (m_fade==2)
			{
				m_fadeBitmap.draw(new BitmapData(1,1,true,0x08000000),null,null,BlendMode.DARKEN);
				this.graphics.beginBitmapFill(m_fadeBitmap);
				this.graphics.drawRect(0, 0, 960, 640);
				this.graphics.endFill();
				if (m_fading<0) m_fade = 0;
			}
		}
		
		public function get createdMapImage():Boolean
		{
			return m_createdMapImage;
		}
	}
}
