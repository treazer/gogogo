﻿package  {
	
	public interface IObserver {

		// Interface methods:
		function update(o: Observable, arg: Object):void;

	}
	
}
