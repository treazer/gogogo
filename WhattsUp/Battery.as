﻿package  {
	
	public class Battery extends Pickup {
		
		public static const RENDER_SIZE_X:int = 64;
		public static const RENDER_SIZE_Y:int = 64;
		
		public function Battery(pos:Vec2, simulation:Simulation) {
			// constructor code
			var _image:String = "battery.png";
			var _size:int = 64;
			m_direction = new Vec2(1,1);
			m_velocity = new Vec2(1,1);
			super(pos, _size, _image, simulation,new Vec2(RENDER_SIZE_X,RENDER_SIZE_Y));
		}
		
		public function step():Boolean {
			if (m_simulation.player.distanceToPoint(position) < size) {
				m_simulation.increaseBatteryCount();
				return true;
			}
			return false;
			
		}

	}
	
}
