﻿package  {
	
	public class Bullet extends GameObject {
		
		private const BULLET_LIFETIME:int = 55;
		private const BULLET_SPEED:Number = 21;
		private var m_damage:int = 10;
		private var m_friendly:Boolean;
		private var life:int;
		private var m_sizeIncrease:Vec2;
		
		public function Bullet(pos:Vec2,velocity:Vec2,type:int,simulation:Simulation,bulletSize:int,renderSizeX:int,renderSizeY:int,bulletImage:String,damage:int,sizeIncrease:Vec2,friendly:Boolean=false,bulletLife:int=BULLET_LIFETIME){
			
			m_damage = damage;
			m_sizeIncrease = sizeIncrease;
			super(pos,bulletSize, bulletImage,simulation,new Vec2(renderSizeX,renderSizeY));
			m_friendly = friendly;
			m_direction = velocity;
			m_velocity = velocity;
			life = bulletLife;
		}
		public function move(){
			this.m_position = this.position.add(m_velocity.scale(BULLET_SPEED));
		}
		
		public function getCurrentField():Vec2
		{
			var x:int = Math.floor(m_position.x/m_simulation.level.tileSizeX);
			var y:int = Math.floor(m_position.y/m_simulation.level.tileSizeY);
			return new Vec2(x,y);
		}
		
		public function step():Boolean {
			life--;
			move();
			setRenderSize(m_renderSize.multiply(m_sizeIncrease));
			if(life<0)
				return true;
			
			//check for hits
			
			//player
			if (!m_friendly)
			{
				if (m_simulation.player.distanceToPoint(m_position)<m_size)
				{
					m_simulation.player.setVelocity(m_simulation.player.velocity.add(m_velocity.normalize().scale(20)));
					m_simulation.player.hit(m_damage);
					return true;
				}
			}
			
			//walls
			if (m_simulation.level.isFieldSolid(getCurrentField()))
			{
				return true;
			}
			//enemies
			if (m_friendly)
			{
				for each(var gameObject:GameObject in m_simulation.gameObjects)
				{
					if (gameObject is Enemy)
					{
						var enemy = Enemy(gameObject);
						if (enemy.distanceToPoint(m_position)<m_size+enemy.size)
						{
						enemy.setVelocity(enemy.velocity.add(m_velocity.normalize().scale(24)));
						enemy.hit(m_damage);
						return true;
						}
					}
				}
			}
			
			return false;
		}
	}
}
