﻿package  {
	
	public class GameButton {

		private var m_pos:Vec2;
		private var m_image:String;
		private var m_size:Vec2;
		private var m_type:String;

		/**
		* GameButton constructor
		* @param bPosition is button position
		* @param bSize is button size
		* @param bImage is path to button background image
		* @param bType is button type
		*/
		
		public function GameButton(bPosition:Vec2, bSize:Vec2, bImage:String, bType:String) {
			
			m_size = bSize;
			m_image = bImage;
			m_type = bType;
			m_pos = bPosition;
		}

		public function get pos():Vec2
		{
			return m_pos;
		}
		public function set pos(newPos:Vec2){
			m_pos = newPos;
		}

		public function get image():String
		{
			return m_image;
		}
		public function set image(newImage:String){
			m_image= newImage;
		}

		public function get size():Vec2
		{
			return m_size;
		}

		public function get type()
		{
			return m_type;
		}
	}
	
}
