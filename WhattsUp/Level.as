﻿package  {
	
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.Event;
	
	public class Level {
		
		private var m_mapdata:Array;
		private var m_functionalData:Array;
		private var m_worldSizeX:int;
		private var m_worldSizeY:int;
		private var m_tileSizeX:int;
		private var m_tileSizeY:int;
		private var m_levelId:int;
		private var m_solidBlocks:Array;
		private var m_loaded:Boolean = false;
		private var m_simulation:Simulation;
		private var m_playerSpawnPosition:Vec2;
		
		private var m_spawnInfoNumEnemies:Array;
		private var m_idsSpawnInfo:Array;
		private var m_idsEnemyNum:Array;
		
		private var m_idsSpawns:Array;
		private var m_idsTrigger:Array;
		private var m_idsMessages:Array;
		private var m_idsScreenShake:Array;
		
		private var m_level_trigger:Vector.<Trigger>;
		private var m_level_spawns:Vector.<Spawn>;
		private var m_messages:Vector.<Message>;
		
		private var fileLoader:URLLoader = new URLLoader();
		private var fileLoaderMessages:URLLoader = new URLLoader();

		public function Level(levelId:int,simulation:Simulation) {
			
			m_levelId = levelId;
			m_simulation = simulation;
			
			fileLoader.addEventListener(Event.COMPLETE,onLoaded);
			fileLoader.load(new URLRequest("assets/mapdata_"+levelId+".txt"));
			
			//tilesheet mapping
			m_solidBlocks = new Array(3,4,5,6,7,8,17,18,19,20,21,22,23,24,25,26,27,36,37,38,39,49,
			50,51,52,53,54,55,56,57,58,59,
			60,61,62,63,64,65,66,67,68,69,
			70,71,72,73,74,75,76,77,78,79,
			81,82,83,84,85,86,87,88,
			97,98,99,100,101,102,103,104,
			113,114,115,116,117,118,119,120,
			129,130,131,132,133,134,135,136,
			145,146,147,148,149,
			200);
			m_idsSpawnInfo = new Array(305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320);
			m_idsEnemyNum  = new Array(321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336);
			m_idsSpawns = new Array(273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288);
			m_idsTrigger = new Array(257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272);
			m_idsMessages = new Array(353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368);
			m_idsScreenShake = new Array(369,370,371);
			
			m_level_trigger = new Vector.<Trigger>();
			m_level_spawns = new Vector.<Spawn>();
			
			m_messages = new Vector.<Message>();
		}
		
		public function getMapdata():Array
		{
			return m_mapdata;
		}
		
		public function get functionalData():Array
		{
			return m_functionalData;
		}
		
		public function isFieldSolid(vec:Vec2):Boolean
		{
			return isTileSolid(m_mapdata[vec.x+vec.y*m_worldSizeX]);
		}
		
		public function isTileSolid(tile:int):Boolean
		{
			var solid:int;
			for each(solid in m_solidBlocks)
			{
				if (solid==tile) return true;
			}
			return false;
		}
		
		public function placeEntities():void
		{
			var value:int = 0;
			var placingPosition:Vec2;
			var trigger:Trigger;

			for (var y:int=0;y<m_worldSizeX;y++)
			{
				for (var x:int=0;x<m_worldSizeY;x++)
				{
					value = m_functionalData[x+m_worldSizeX*y];

					//create position vector
					if (value!=0)
					{
						placingPosition = new Vec2(x*m_tileSizeX,y*tileSizeY);
					}
					
					if (value==289) //spawn
					{
						m_playerSpawnPosition = placingPosition;
						m_simulation.player.setPosition(m_playerSpawnPosition);
					}
					if (value==290) //game end trigger
					{
						trigger = new Trigger(m_simulation,placingPosition,45,true);
						m_simulation.trigger.push(trigger);
						m_simulation.triggermap[x+m_worldSizeX*y]=trigger;
					}
					if (value==291) //battery
					{
						var battery:Battery = new Battery(placingPosition.add(new Vec2(Battery.RENDER_SIZE_X/2,Battery.RENDER_SIZE_Y/2)), m_simulation);
						m_simulation.gameObjects.push(battery);
					}
					
					//enemy spawn information
					if (value==305)
					{
						readSpawnInformation(x,y);
					}
					
					//spawns
					for each (var index:int in m_idsSpawns)
					{
						//trace("check for index:"+index+" with value:"+value);
						if (index==value)
						{

							var spawn:Spawn = new Spawn(placingPosition,m_simulation,m_idsSpawns.indexOf(value)+1,m_spawnInfoNumEnemies[m_idsSpawns.indexOf(value)+1]);
							m_simulation.spawner.push(spawn);
							m_level_spawns.push(spawn);

						}
					}
					
					//trigger
					for each (index in m_idsTrigger)
					{
						if (index==value)
						{
							trigger = new Trigger(m_simulation,placingPosition,43,false,m_idsTrigger.indexOf(value)+1);
							m_simulation.trigger.push(trigger);
							m_level_trigger.push(trigger);
							m_simulation.triggermap[x+m_worldSizeX*y]=trigger;

						}
					}
					
					//message
					for each (index in m_idsMessages)
					{
						if (index==value)
						{
							var newMessage = createNewMessage(m_idsMessages.indexOf(value)+1);
							trigger = new MessageTrigger(m_simulation,placingPosition,40,m_idsTrigger.indexOf(value)+1,newMessage);
							m_simulation.trigger.push(trigger);
							m_simulation.messages.push(newMessage);
							m_messages.push(newMessage);
						}
					}
					
					//screenshake
					for each (index in m_idsScreenShake)
					{
						if (index==value)
						{
							if (index==369)
								trigger = new TriggerShake(m_simulation,placingPosition,75,3,50);
							if (index==370)
								trigger = new TriggerShake(m_simulation,placingPosition,75,4,70);
							if (index==371)
								trigger = new TriggerShake(m_simulation,placingPosition,75,5,90);
							m_simulation.trigger.push(trigger);
						}
					}
				}
			}
			
			linkSpawnsToTrigger();
		}
		
		public function createNewMessage(id:int):Message
		{
			for each (var msg:Message in m_messages)
			{
				if (msg.id==id)
					return msg;
			}
			return new Message("msg",id);
		}
		
		public function linkSpawnsToTrigger():void
		{
			for each (var trigger:Trigger in m_level_trigger)
			{
				for each (var spawn:Spawn in m_level_spawns)
				{
					if (spawn.id==trigger.id)
					{
						trigger.addSpawn(spawn);
					}
				}
			}
		}
		
		public function readSpawnInformation(x:int,y:int):void
		{
			m_spawnInfoNumEnemies = new Array();
			var value:int;
			for (var pos:int=0;pos<16;pos++)
			{
				value = m_functionalData[x+pos+m_worldSizeX*(y+1)];
				m_spawnInfoNumEnemies.push(m_idsEnemyNum.indexOf(value)+1);
				//trace("for nr:"+pos+" ->"+m_spawnInfoNumEnemies[pos]+" for value"+value);
			}
		}
		
		public function onLoaded(e:Event):void
		{
			fileLoader.removeEventListener(Event.COMPLETE,onLoaded);
			
			var startRead:int = 0;
			var endRead:int = 0;
			var reachedEnd:Boolean = false;
			var line:String;
			var content:String = e.target.data.toString();
			var dataPointer:int = 0;
			
			//width
			startRead = content.indexOf("=",startRead)+1;
			endRead = content.indexOf("h",startRead);
			line = content.substring(startRead,endRead-1);
			m_worldSizeX = int(line);
			
			startRead = endRead+1;
			
			//height
			startRead = content.indexOf("=",startRead)+1;
			endRead = content.indexOf("t",startRead);
			line = content.substring(startRead,endRead-1);
			m_worldSizeY = int(line);
			
			startRead = endRead+1;
			
			//tilewidth
			startRead = content.indexOf("=",startRead)+1;
			endRead = content.indexOf("t",startRead);
			line = content.substring(startRead,endRead-1);
			m_tileSizeX = int(line);
			
			startRead = endRead+1;
			
			//tileheight
			startRead = content.indexOf("=",startRead)+1;
			endRead = content.indexOf("[",startRead);
			line = content.substring(startRead,endRead-2);
			m_tileSizeY = int(line);

			//mapdata
			m_mapdata = new Array(worldSizeX*worldSizeY);
			startRead = content.indexOf("data=",startRead)+5;
			while(!reachedEnd)
			{
				endRead = content.indexOf(",",startRead);
				line = content.substring(startRead,endRead);
				m_mapdata[dataPointer]= int(line);
				startRead = endRead+1;
				
				dataPointer++;
				
				if (dataPointer>worldSizeX*worldSizeY)
					reachedEnd=true;
			}	
			
			//functional layer
			dataPointer = 0;
			
			m_functionalData = new Array(worldSizeX*worldSizeY);
			startRead = content.indexOf("type=functional",0);
			startRead = content.indexOf("=",startRead)+18;
			reachedEnd = false;
			while(!reachedEnd)
			{
				endRead = content.indexOf(",",startRead);
				line = content.substring(startRead,endRead);
				m_functionalData[dataPointer]= int(line);
				startRead = endRead+1;
				
				dataPointer++;
				
				if (dataPointer>worldSizeX*worldSizeY)
					reachedEnd=true;
			}	
			
			placeEntities();
			
			fileLoaderMessages.addEventListener(Event.COMPLETE,onLoadedMessages);
			fileLoaderMessages.load(new URLRequest("assets/messages_"+m_levelId+".txt"));
		}
		
		public function onLoadedMessages(e:Event):void
		{
			fileLoaderMessages.removeEventListener(Event.COMPLETE,onLoadedMessages);
			
			var startRead:int = 0;
			var endRead:int = 0;
			var reachedEnd:Boolean = false;
			var line:String;
			var content:String = e.target.data.toString();
			var message_id:int;
			var message_content:String;
			
			//id
			while(!reachedEnd)
			{
			endRead = content.indexOf(":",startRead);
			line = content.substring(startRead,endRead);
			message_id = int(line);
			startRead = endRead+1;
			
			//content
			startRead = endRead+2;
			endRead = content.indexOf(";",startRead);
			line = content.substring(startRead,endRead-1);
			message_content = line;
			startRead=endRead+1;

			//add message content
			addMessageContent(message_id,message_content);
			if (startRead+1>content.length)
				reachedEnd=true;
			}
			m_loaded = true;
		}
		
		public function addMessageContent(id:int,content:String)
		{
			for each (var msg:Message in m_messages)
			{
				if (msg.id==id)
					msg.setContent(content);
			}
		}
		
		public function get tileSizeX():int
		{
			return m_tileSizeX;
		}
		
		public function get tileSizeY():int
		{
			return m_tileSizeY;
		}
		
		public function get worldSizeX():int
		{
			return m_worldSizeX;
		}
		
		public function get worldSizeY():int
		{
			return m_worldSizeY;
		}
		
		public function get hasLoaded():Boolean
		{
			return m_loaded;
		}
	}
}
