﻿package  {
	import flash.system.MessageChannel;
	
	public class MessageTrigger extends Trigger {

		private var m_message:Message;
		
		public function MessageTrigger(simulation:Simulation,position:Vec2,radius:int,id:int,pMessage:Message) {
			m_message = pMessage;
			super(simulation,position,radius,false,id);
		}
		
		override public function step():void
		{
			if (m_simulation.player.distanceToPoint(m_position) < m_radius)
			{
				notifyMessage();
			}
			
		}

		public function notifyMessage():void
		{
			m_message.showMessage();
		}
	}
	
}
