﻿package  {
	
	public class Vec2 {
		
		private var m_x:Number;
		private var m_y:Number;
		private var lineLength:Number;
		private var inverseLength:Number;
		private var lineNormalX:Number;
		private var lineNormalY:Number;
		
		public function Vec2(pX:Number,pY:Number) {
			m_x = pX;
			m_y = pY;
			lineLength = Math.sqrt((m_x*m_x)+(m_y*m_y));
			inverseLength = 1/lineLength;

			lineNormalX = inverseLength*m_x;
			lineNormalY = inverseLength*m_y;
		}
		
		public function get x():Number {
			return m_x;
		}
		
		public function get y():Number {
			return m_y;
		}
		
		public function setX(value:Number) {
			m_x = value;
		}
		
		public function setY(value:Number) {
			m_y = value;
		}

		public function add(v:Vec2):Vec2 {
			return new Vec2(m_x+v.x,m_y+v.y);
		}
		
		public function sub(v:Vec2):Vec2 {
			return new Vec2(m_x-v.x,m_y-v.y);
		}
		
		public function toString():String {
			return "[Vec2 "+m_x+","+m_y+"]";
		}
		
		public function multiply(v:Vec2):Vec2 {
			return new Vec2(m_x*v.x,m_y*v.y);
		}
		
		public function divide(v:Vec2):Vec2 {
			return new Vec2(m_x/v.x,m_y/v.y);
		}
		
		public function clone():Vec2 {
			return new Vec2(m_x,m_y);
		}
		
		public function scale(scalar:Number):Vec2
		{
			return new Vec2(m_x*scalar,m_y*scalar);
		}
		
		public function get length():Number
		{
			return lineLength;
		}
		
		public function mirror(horizontal:Boolean)
		{
			if (horizontal) m_y*=-1; else m_x*=-1;
		}
		
		public function normalize():Vec2
		{
			return new Vec2(lineNormalX,lineNormalY);
		}
		
		public function clip(x:int,y:int,width:int,height:int):Vec2
		{
			var pX:Number = m_x;
			var pY:Number = m_y;
			if (m_x < x) pX = x;
			if (m_y < y) pY = y;
			if (m_x > x + width) pX = x + width;
			if (m_y > y + height) pY = y + height;
			return new Vec2(pX,pY);
		}
		
		public function reflect(vector:Vec2):Vec2
		{
			var dot:Number = this.dot(vector);
			var normal:Vec2 = this.normalize();
			normal = normal.scale(dot*2);
			return vector.sub(normal);
		}
		
		public function dot(vector:Vec2):Number {
			return vector.x * m_x + vector.y * m_y;
		}
		
		public function getNormal():Vec2
		{
			return new Vec2(-m_y,m_x).normalize();
		}
	
	}
	
}
