﻿package  {
	import flash.display.Bitmap;
	import flash.events.Event;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.display.Bitmap;
	import flash.display.*;


	
	public class ImageLoader {
		
		public static const STANDART_BUTTON_SIZE:Vec2 = new Vec2(315,63);
		public static const STANDART_LVL_SELEC_SITZE:Vec2 = new Vec2 (86,82);
		public static const STANDART_BACKGROUND_SIZE:Vec2 = new Vec2 (960,640);
		
		public static const IMAGE_TILEMAP:String = "tilemap.png";
		public static const IMAGE_DEFAULT:String = "default.png";
		public static const IMAGE_PLAYER:String = "player.png";
		public static const IMAGE_ENEMY_1:String = "enemy1.png";
		public static const IMAGE_BATTERY:String = "battery.png";
		public static const IMAGE_BULLET_DEFAULT:String = "bulletdefault.png";
		public static const IMAGE_BULLET:String = "bullet.png";
		public static const IMAGE_BULLET_SPITTER:String = "bullet_3.png";
		public static const IMAGE_BULLET_FRYBYE:String = "bullet_2.png";
		public static const IMAGE_WEAPONICON_1:String = "weapon1icon.png";
		public static const IMAGE_WEAPONICON_2:String = "weapon2icon.png";
		public static const IMAGE_LIGHT:String = "light.png";
		public static const IMAGE_PLAYER_LEGS:String = "player_legs.png";
		public static const IMAGE_LOADING:String = "loading.png";
		public static const IMAGE_SELECT_LEVEL:String = "SelectLevel.png";
		public static const IMAGE_OPTIONS:String = "Options.png";
		public static const IMAGE_HELP:String = "Help.png";
		public static const IMAGE_MAINBACKGROUND:String = "MainBackground.png";
		public static const IMAGE_LEVEL_1:String = "level_1.png";
		public static const IMAGE_LEVEL_2:String = "level_2.png";
		public static const IMAGE_LEVEL_3:String = "level_3.png";
		public static const IMAGE_LEVEL_4:String = "level_4.png";
		public static const IMAGE_LEVEL_5:String = "level_5.png";
		public static const IMAGE_LEVEL_6:String = "level_6.png";
		public static const IMAGE_LEVEL_7:String = "level_7.png";
		public static const IMAGE_LEVEL_8:String = "level_8.png";
		public static const IMAGE_LEVEL_9:String = "level_9.png";
		public static const IMAGE_LEVEL_LOCKED:String = "locked_level.png";
		public static const IMAGE_BACK:String = "Back.png";
		public static const IMAGE_PAUSE:String = "Pause.png";
		public static const IMAGE_PAUSEBACKGROUND:String = "PauseBackground.png";
		public static const IMAGE_PLAYER_HIT:String = "playerHit.png";
		public static const IMAGE_MESSAGE:String = "messageBox.png";
		public static const IMAGE_SLIDE_BAR:String = "senstivity_bar.png";
		public static const IMAGE_SLIDE_BAR_MARKER:String ="sensivity_marker.png";
		public static const IMAGE_MENU_OPTIONS:String = "MenuOptions.png";
		public static const IMAGE_MENU_HELP:String = "MenuHelp.png";
		public static const IMAGE_YES:String = "Yes.png";
		public static const Image_NO:String = "No.png";
		public static const IMAGE_BLUEBAR:String = "bluebar.png";
		public static const IMAGE_YELLOWBAR:String = "yellowbar.png";
		public static const IMAGE_GAME_OVER:String = "gameOver.png";
		public static const IMAGE_LEVEL_DONE:String = "levelDone.png";
		public static const IMAGE_QUIT:String = "Quit.png";
		public static const IMAGE_RESUME:String = "Resume.png";
		
		private static var m_tilemapBitmap:Bitmap;
		private static var m_loadingComplete:Boolean = false;
		private static var ldr:Loader = new Loader();
		private static var images:Array = new Array(IMAGE_TILEMAP,IMAGE_DEFAULT,IMAGE_PLAYER,IMAGE_ENEMY_1,
													IMAGE_BATTERY,IMAGE_BULLET,IMAGE_BULLET_DEFAULT,IMAGE_BULLET_SPITTER,
													IMAGE_BULLET_FRYBYE,IMAGE_WEAPONICON_1,IMAGE_WEAPONICON_2,
													IMAGE_LIGHT,IMAGE_PLAYER_LEGS,IMAGE_SELECT_LEVEL,IMAGE_LOADING,IMAGE_OPTIONS,
													IMAGE_HELP,IMAGE_BACK,IMAGE_MAINBACKGROUND,IMAGE_LEVEL_1,IMAGE_LEVEL_2,IMAGE_LEVEL_3,
													IMAGE_LEVEL_4,IMAGE_LEVEL_5,IMAGE_LEVEL_6,IMAGE_LEVEL_7,IMAGE_LEVEL_8,IMAGE_LEVEL_9,
													IMAGE_LEVEL_LOCKED,IMAGE_PAUSE,IMAGE_PAUSEBACKGROUND,IMAGE_PLAYER_HIT,IMAGE_MESSAGE,
													IMAGE_SLIDE_BAR,IMAGE_SLIDE_BAR_MARKER,IMAGE_MENU_OPTIONS,IMAGE_MENU_HELP,IMAGE_YES,Image_NO,
													IMAGE_YELLOWBAR, IMAGE_BLUEBAR,IMAGE_GAME_OVER,IMAGE_LEVEL_DONE,IMAGE_QUIT,IMAGE_RESUME);
		private static var bitmapDatas:Array = new Array();
		private static var currentIndex:int = 0;
		
		public function ImageLoader() {
			
		}
		
		public static function loadAllGameImages()
		{
			currentIndex = 0;
			loadImage(images[currentIndex]);
		}
		
		public static function loadImage(imagePath:String)
		{
			ldr.contentLoaderInfo.addEventListener(Event.COMPLETE, ldr_complete);
			ldr.load(new URLRequest("assets/images/"+imagePath));
		}
		
		private static function ldr_complete(e:Event):void {
			
			ldr.contentLoaderInfo.removeEventListener(Event.COMPLETE, ldr_complete);
			var bmp:Bitmap = ldr.content as Bitmap;
 
			bitmapDatas[currentIndex] = new Bitmap(bmp.bitmapData);

			
			currentIndex++;
			if (currentIndex<images.length)
			{
				loadImage(images[currentIndex])
			}
			else
			{
				m_loadingComplete = true;
			}
		}
		
		public static function get tilemapBitmap():Bitmap
		{
			return m_tilemapBitmap;
		}
		
		public static function isDoneLoading():Boolean
		{
			return m_loadingComplete;
		}
		
		public static function getBitmapDataFromName(imageName:String):BitmapData
		{
			var index:int=0;
			while (imageName!=images[index])
				index++;
			
			return bitmapDatas[index].bitmapData;
		}

	}
	
}
