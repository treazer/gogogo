﻿package  {
	
	import flash.display.Graphics;
	import flash.display.MovieClip;
	import flash.geom.Rectangle;
	import flash.geom.Matrix;
	
	public class GameMenu extends MovieClip{
		
		private var menuObjects:Array = new Array;
		
		private var menueState:String
		private var m_gameStateManager:GameStateManager;
		private var sensitivity_bar:GameButton = new GameButton(new Vec2(320,235),new Vec2(278,40),"senstivity_bar.png","SenstivityBar");
		private var sensitivity_marker:MenuImage = new MenuImage(new Vec2((Options.ACCELEROMETER_SCALER.x/5) * sensitivity_bar.size.x +sensitivity_bar.pos.x,240),new Vec2(44,29),"sensivity_marker.png","SensitivityMarker");
		private var sound:GameButton;
		
		public function GameMenu(gameStateManager:GameStateManager,State:String) {
			menuObjects = new Array;
			m_gameStateManager = gameStateManager;
			gameStateManager.main.stage.addChild(this);
			
			menueState = State;
			if(menueState == "Main"){
				createMain();
			}else if(menueState == "GameOver"){
				createGameOver();
			}else if(menueState == "Win"){
				createWin();
			}
			
			if(Options.SOUND == true){
				sound =(new GameButton(new Vec2(500,400),new Vec2(86,22),"Yes.png","Sound"));
			}else{
				sound=(new GameButton(new Vec2(500,400),new Vec2(86,22),"No.png","Sound"));
			}
		}
		public function clickAt(pos:Vec2):int{
			
			switch(menueState){
				case "Main":
						switch(getclosestObject(pos)){
							case "SelectLevel":
								menueState = getclosestObject(pos);
								deletMenuObjects();
								createLevelSelect();
							break;
							case "Options":
								menueState = getclosestObject(pos);
								deletMenuObjects();
								createOptions();
							break;
							case "Help":
								menueState = getclosestObject(pos);
								deletMenuObjects();
								createHelp();
							break;
						}
				break;
				case "Options":
					switch(getclosestObject(pos)){
						case "Main":
							menueState = getclosestObject(pos);
							deletMenuObjects();
							createMain();
						break;
						case "SenstivityBar":
							sensitivity_marker.pos = new Vec2(pos.x - sensitivity_marker.size.x / 2, sensitivity_marker.pos.y);
							Options.ACCELEROMETER_SCALER = new Vec2(5,5).scale((pos.x - sensitivity_bar.pos.x)/sensitivity_bar.size.x);
						break;
						case"Sound":
							if(Options.SOUND == true){
								Options.SOUND = false;
								sound.image = "No.png"
								
							}else{
								Options.SOUND = true;
								sound.image = "Yes.png";
							}
						break;
					}
				break;
				case "Help":
					switch(getclosestObject(pos)){
						case "Main":
							menueState = getclosestObject(pos);
							deletMenuObjects();
							createMain();
						break;
					}
				break;
				case "SelectLevel":
					if (getclosestObject(pos)=="Main")
						{
							menueState = getclosestObject(pos);
							deletMenuObjects();
							createMain();
						}
					var dummy:int = new Number(getclosestObject(pos));
					if(dummy != 0){
						m_gameStateManager.switchToRunning(dummy);
						return 1;
						
					}				
				break;
				case "GameOver":
					if(getclosestObject(pos) == "Main"){
						menueState = getclosestObject(pos);
						deletMenuObjects();
						createMain();
					}
				break;
				case "Win":
					if(getclosestObject(pos) == "Main"){
						menueState = getclosestObject(pos);
						deletMenuObjects();
						createMain();
					}
				break;
			}
			
			return 0;
			
		}
		public function remove(){
			m_gameStateManager.main.stage.removeChild(this);
			deletMenuObjects();
		}
		private function getclosestObject(pos:Vec2):String{
			var wasClickt:String = "None";
			for(var i = 0; i < menuObjects.length; i++){
				if(menuObjects[i] is GameButton){
					if((menuObjects[i].pos.x < pos.x)&&(menuObjects[i].pos.x + menuObjects[i].size.x > pos.x )){
						if((menuObjects[i].pos.y < pos.y)&&(menuObjects[i].pos.y + menuObjects[i].size.y > pos.y )){
							
							wasClickt = menuObjects[i].type;
							trace(wasClickt);
						}
					}
				}
				
			}
			return wasClickt;
		}
		public function step(){
			draw();
			
		}
		public function draw(){
			if(ImageLoader.isDoneLoading()){
				graphics.clear();
				for(var i = 0; i < menuObjects.length; i++){
					var matrix:Matrix = new Matrix();
					matrix.translate(menuObjects[i].pos.x,menuObjects[i].pos.y);
					this.graphics.beginBitmapFill(ImageLoader.getBitmapDataFromName(menuObjects[i].image), matrix);
					this.graphics.drawRect(menuObjects[i].pos.x,menuObjects[i].pos.y,menuObjects[i].size.x,menuObjects[i].size.y);
					this.graphics.endFill();
				}
			}
		}
		private function createMain(){
			menuObjects.push(new MenuImage(new Vec2(0,0),ImageLoader.STANDART_BACKGROUND_SIZE,"MainBackground.png","Background"));
			menuObjects.push(new GameButton(new Vec2(320,236),ImageLoader.STANDART_BUTTON_SIZE,"SelectLevel.png","SelectLevel"));
			menuObjects.push(new GameButton(new Vec2(320,336),ImageLoader.STANDART_BUTTON_SIZE,"Options.png","Options"));
			menuObjects.push(new GameButton(new Vec2(320,436),ImageLoader.STANDART_BUTTON_SIZE,"Help.png","Help"));
		}
		private function createLevelSelect(){
			menuObjects.push(new MenuImage(new Vec2(0,0),ImageLoader.STANDART_BACKGROUND_SIZE,"MainBackground.png","Background"));
			menuObjects.push(new GameButton(new Vec2(310,230),ImageLoader.STANDART_LVL_SELEC_SITZE,"level_1.png","1"));
			menuObjects.push(new GameButton(new Vec2(430,230),ImageLoader.STANDART_LVL_SELEC_SITZE,"level_2.png","2"));
			menuObjects.push(new GameButton(new Vec2(550,230),ImageLoader.STANDART_LVL_SELEC_SITZE,"level_3.png","3"));
			menuObjects.push(new GameButton(new Vec2(310,330),ImageLoader.STANDART_LVL_SELEC_SITZE,"level_4.png","4"));
			menuObjects.push(new GameButton(new Vec2(430,330),ImageLoader.STANDART_LVL_SELEC_SITZE,"level_5.png","5"));
			menuObjects.push(new GameButton(new Vec2(550,330),ImageLoader.STANDART_LVL_SELEC_SITZE,"level_6.png","6"));
			menuObjects.push(new GameButton(new Vec2(320,436),ImageLoader.STANDART_BUTTON_SIZE,"Back.png","Main"));
		
		}
		private function createHelp(){
			menuObjects.push(new MenuImage(new Vec2(0,0),ImageLoader.STANDART_BACKGROUND_SIZE,"MenuHelp.png","Help"));
			menuObjects.push(new GameButton(new Vec2(330,500),ImageLoader.STANDART_BUTTON_SIZE,"Back.png","Main"));
		}
		private function createOptions(){
			menuObjects.push(new MenuImage(new Vec2(0,0),ImageLoader.STANDART_BACKGROUND_SIZE,"MenuOptions.png","Options"));
			menuObjects.push(new GameButton(new Vec2(330,500),ImageLoader.STANDART_BUTTON_SIZE,"Back.png","Main"));
			menuObjects.push(sound);
			menuObjects.push(sensitivity_bar);
			menuObjects.push(sensitivity_marker);
		}
		private function createGameOver(){
			menuObjects.push(new MenuImage(new Vec2(0,0),ImageLoader.STANDART_BACKGROUND_SIZE,"gameOver.png","GameOver"));
			menuObjects.push(new GameButton(new Vec2(960/2 - 315 / 2,330),new Vec2(315,63),"Quit.png","Main"));
		}
		private function createWin(){
			menuObjects.push(new MenuImage(new Vec2(0,0),new Vec2(315,63),"levelDone.png","Win"));
			menuObjects.push(new GameButton(new Vec2(960/2 - 315 / 2,330),new Vec2(315,63),"Quit.png","Main"));
		}
		private function deletMenuObjects(){
			menuObjects.length = 0;
		}


	}
	
}
