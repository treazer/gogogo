﻿package  {
	
	import flash.display.*;
	import flash.display.Bitmap;
	import flash.geom.Rectangle;
	import flash.geom.Point;
	import flash.filters.*;
	import flash.geom.Matrix;
	
	public class DiffuseMap {

		private var m_diffuseMap:BitmapData;
		
		public function DiffuseMap() {
			// constructor code
		}
		
		public function get diffuseMap():BitmapData
		{
			return m_diffuseMap;
		}
		
		public function createDiffuseMap(level:Level)
		{
			m_diffuseMap = new BitmapData(level.worldSizeX*level.tileSizeX,level.worldSizeY*level.tileSizeY,false,0x000000);
			var tempRect:Rectangle = new Rectangle(0,0,level.tileSizeX,level.tileSizeY);
			
			for (var tileX:int=0;tileX<level.worldSizeX;tileX++)
			{
				for (var tileY:int=0;tileY<level.worldSizeY;tileY++)
				{
					m_diffuseMap.copyPixels(getTileFromMap(level.getMapdata()[tileX+(tileY*level.worldSizeX)],level),tempRect,new Point(tileX*level.tileSizeX,tileY*level.tileSizeY));
				}
			}
		}
		
		public function getTileFromMap(tileNr:int,level:Level):BitmapData
		{
			var tileY:int = Math.floor(tileNr/16.1);
			var tileX:int = (tileNr-tileY*16)-1;

			var tempData:BitmapData = new BitmapData(level.tileSizeX,level.tileSizeY,false);
			var tempRect = new Rectangle(tileX*level.tileSizeX,tileY*level.tileSizeY,level.tileSizeX,level.tileSizeY);
			tempData.copyPixels(ImageLoader.getBitmapDataFromName(ImageLoader.IMAGE_TILEMAP),tempRect,new Point(0,0)); //tilemapBitmap.bitmapData
			return tempData;
			
		}
	}
	
}
