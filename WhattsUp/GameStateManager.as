﻿package  {
	
	import flash.utils.Timer;
	import flash.events.*;
	import flash.text.engine.GraphicElement;
	import flash.text.engine.TabAlignment;
	
	public class GameStateManager extends Observable {

		private var m_main:Main;
		private var m_simulation:Simulation;
		private var m_gameState:String = "Menu";
		private var m_soundManager:SoundManager;
		private var m_menu:GameMenu;
		public var m_pause:PauseMenu;
		
		public function GameStateManager(main:Main) {
			
			m_main = main;
			m_soundManager = new SoundManager(m_main);
			ImageLoader.loadAllGameImages();
			creatMenue("Main");
			
			SoundManager.playMusic("assets/sounds/noise.wav");
			
		}

		public function step():void
		{
			switch(m_gameState){
				case "Running":
					gameRunning();
				break;
				case "Menu":
					updateMenu();
				break;
				case "Pause":
					updatePause();
				break;
			}
		}
		public function switchToPause(){
			m_gameState = "Pause";
			m_pause = new PauseMenu(this);
		}
		public function pauseToRunning(){
			m_gameState = "Running";
			m_pause.remove();
			m_pause = null;
		}
		public function switchToRunning(level:int){
			creatSimulation(level);
			m_gameState = "Running";
			m_menu.remove();
			m_menu = null;
			
		}
		public function gameRunning(){
			m_simulation.step();
			notifyObservers();
		}
		public function updateMenu(){
			m_menu.step();
		}
		public function updatePause(){
			m_pause.step();
		}
		public function creatMenue(state:String){
			m_gameState = "Menu";
			m_menu = new GameMenu(this,state);
			
		}
		public function removeSimulation(){
			m_simulation = null;
		}
		public function creatSimulation(level:int){
			m_simulation = new Simulation(this,m_main,level);
		}
		public function get main():Main{
			return m_main;
		}
		
		public function get simulation()
		{
			return m_simulation;
		}
		public function get gameState():String{
			return m_gameState;
		}
		public function get menu(){
			return m_menu;
		}
		public function get soundManager():SoundManager
		{
			return m_soundManager;
		}
		public function endGame(){
			
		}
	}
}
