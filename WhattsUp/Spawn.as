﻿package  {
	
	public class Spawn {
		
		private var m_position:Vec2;
		private var m_numberOfSpawns:int;
		private var m_simulation:Simulation;
		private var m_triggered:Boolean;
		private var m_id:int;

		public function Spawn(position:Vec2,simulation:Simulation,id:int,numberOfSpawns:int=1) { //position is given as tile-coordinate
			m_position = new Vec2(position.x,position.y);
			m_simulation = simulation;
			m_numberOfSpawns = numberOfSpawns;
			m_triggered=false;
			m_id = id;
		}
		
		public function step():void
		{
			
			if (m_triggered && m_numberOfSpawns > 0)
			{
				//spawn enemies
				var enemy = new Enemy(m_position,m_simulation);
				//m_simulation.enemies.push(enemy);
				m_simulation.gameObjects.push(enemy);
				m_numberOfSpawns--;
				
			}
		}
		
		public function trigger():void
		{
			m_triggered = true;
		}
		
		public function get id():int
		{
			return m_id;
		}
		
	}
}
